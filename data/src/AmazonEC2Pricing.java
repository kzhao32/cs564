import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;


public class AmazonEC2Pricing {

	File excelFile = new File(System.getProperty("user.dir") + "/Amazon EC2 Pricing Data.xlsx");
	String[] excelHeader = { "Size", "Type", "vCPU", "ECU", "Memory (GiB)", "Instance Storage (GB)", "Usage" };
	String[] excelRow = { "size", "type", "vCPU", "ECU", "memory", "storage", "rate" };

	// all below is case sensitive
	String[] operatingSystems = { "Linux", "Windows" }; // match tab on website
	String[] regions = { "US East (N. Virginia)", "US West (Northern California)" };
	String[] types = { "General Purpose - Current Generation", "Compute Optimized - Current Generation", "GPU Instances - Current Generation", "Memory Optimized - Current Generation", "Storage Optimized - Current Generation" };
	String[] generalPurposeInstances = { "t2.micro", "t2.small", "t2.medium", "m3.medium", "m3.large", "m3.xlarge", "m3.2xlarge" };
	String[] computeOptimizedInstances = { "c3.large", "c3.xlarge", "c3.2xlarge", "c3.4xlarge", "c3.8xlarge" };
	String[] GPUInstances = { "g2.2xlarge" };
	String[] memoryOptimizedInstances = { "r3.large", "r3.xlarge", "r3.2xlarge", "r3.4xlarge", "r3.8xlarge" };
	String[] storageOptimizedInstances = { "i2.xlarge", "i2.2xlarge", "i2.4xlarge", "i2.8xlarge", "hs1.8xlarge" };
	//int numberOfInstances = generalPurposeInstances.length + computeOptimizedInstances.length + GPUInstances.length + memoryOptimizedInstances.length + storageOptimizedInstances.length;	
	String[][] instancess = { generalPurposeInstances, computeOptimizedInstances, GPUInstances, memoryOptimizedInstances, storageOptimizedInstances };

	Map<String, String> rates = new HashMap<String, String>();
	Map<String, String> usage = new HashMap<String, String>();

	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public static void main(String[] args) {
//		AmazonEC2Pricing instance = new AmazonEC2Pricing();
//		instance.setMap();
//		instance.start();		
	}

//	public void start() {
//
//		System.out.println(updateExcelFile());
//		System.out.println("done on " + dateFormat.format(new Date()));
//		System.exit(0);
//		//		WebDriver webDriver = new FirefoxDriver();
//		//		webDriver.get("http://aws.amazon.com/ec2/pricing/");
//		//		while (true) {
//		//			for (String operatingSystem : operatingSystems) {
//		//				webDriver.findElement(By.xpath("//a[text()='" + operatingSystem + "']")).click();
//		//				
//		//			}
//		//			break;
//		//		}
//		//		
//		//		
//		//		//webDriver.findElement(By.xpath("//a[text()='Windows']")).click();
//		//		//sleep(5); // wait for prices to load
//		//		//String t2Micro = webDriver.findElement(By.xpath("//th[text()='General Purpose - Current Generation']/../..//td[@class='size'][]")).getText();
//		//		String t2Micro = webDriver.findElement(By.xpath("//caption[text()='US East (N. Virginia)']/..//td[text()='t2.micro']/../td[@class='rate mswin']")).getText();
//		//		System.out.println(t2Micro);
//		//		webDriver.findElement(By.xpath("//a[text()='Linux']")).click();
//		//		String t1Micro = webDriver.findElement(By.xpath("//caption[text()='US East (N. Virginia)']/..//td[text()='t2.micro']/../td[@class='rate linux']")).getText();
//		//		System.out.println(t1Micro);
//		//		
//		//		webDriver.findElement(By.xpath("//div[contains(@id, 'aws-element')][contains(@class, 'active')]//span[@class='dropdown-label']")).click();
//		//		webDriver.findElement(By.xpath("//div[@class='dropdown-list']//span[text()='" + "US West (Northern California)" + "']")).click();
//		//		
//		//		String t3Micro = webDriver.findElement(By.xpath("//caption[text()='US West (Northern California)']/..//td[text()='t2.micro']/../td[@class='rate linux']")).getText();
//		//		System.out.println(t3Micro);
//		//		
//		//		System.out.println("done on " + dateFormat.format(new Date()));
//	}
//
//	public void setMap() {
//		//		for (int i = 0; i < instancess.length; i++) {
//		//			for (int j = 0; j < instancess[i].length; j++) {
//		//				System.out.println("i, j, value = " + i + " " + j + " " + instancess[i][j]);
//		//			}
//		//		}
//		rates.put("Linux", "rate linux");
//		rates.put("RHEL", "rate rhel");
//		rates.put("SLES", "rate sles");
//		rates.put("Windows", "rate mswin");
//		rates.put("Windows with SQL Standard", "rate mswinSQL");
//		rates.put("Windows with SQL Web", "rate mswinSQLWeb");
//		usage.put("Linux", "Linux/UNIX Usage");
//		usage.put("RHEL", "Red Hat Enterprise Linux");
//		usage.put("SLES", "SUSE Linux Enterprise Server Usage");
//		usage.put("Windows", "Windows Usage");
//		usage.put("Windows with SQL Standard", "Windows with SQL Standard Usage");
//		usage.put("Windows with SQL Web", "Windows with SQL Web Usage");
//	}
//
//	//	public String[] getInstanceRates() {
//	//		String[] rates = new String[operatingSystems.length];
//	//		for (int i = 0; i < operatingSystems.length; ++i) {
//	//			if (operatingSystems[i].equals("Linux")) {
//	//				rates[i] = "rate linux";
//	//			} else if (operatingSystems[i].equals("RHEL")) {
//	//				rates[i] = "rate rhel";
//	//			} else if (operatingSystems[i].equals("SLES")) {
//	//				rates[i] = "rate sles";
//	//			} else if (operatingSystems[i].equals("Windows")) {
//	//				rates[i] = "rate mswin";
//	//			} else if (operatingSystems[i].equals("Windows with SQL Standard")) {
//	//				rates[i] = "rate mswinSQL";
//	//			} else if (operatingSystems[i].equals("Windows with SQL Web")) {
//	//				rates[i] = "rate mswinSQLWeb";
//	//			} else {
//	//				System.err.println("error: no match for operating system. program is coded for");
//	//				System.err.println("\tLinux");
//	//				System.err.println("\tRHEL");
//	//				System.err.println("\tSLES");
//	//				System.err.println("\tWindows");
//	//				System.err.println("\tWindows with SQL Standard");
//	//				System.err.println("\tWindows with SQL Web");
//	//				System.exit(-1);
//	//			}
//	//		}
//	//		return rates;
//	//	}
//
//	public int updateExcelFile() {
//		int numberOfColumns = 0;
//		Sheet sheet = null;
//		CellStyle cellStyle = null;
//		CellStyle boldCellStyle = null;
//		Row row;
//		Cell cell;
//		int rowCounter = 0;
//		List<WebElement> webElements;
//		Workbook workbook = null;
//		
//		WebDriver webDriver = new FirefoxDriver();
//		webDriver.get("http://aws.amazon.com/ec2/pricing/");
//
//		if (excelFile.exists()) {
//			FileInputStream fileInputStream;
//			try {
//				fileInputStream = new FileInputStream(excelFile);
//				workbook = new XSSFWorkbook(fileInputStream);
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//				return -1;
//			} catch (IOException e) {
//				e.printStackTrace();
//				return -2;
//			}
//			sheet = workbook.getSheetAt(0);
//			Iterator<Row> rowIterator = sheet.iterator();
//			if (rowIterator.hasNext()) {
//				row = rowIterator.next();
//				Iterator<Cell> cellIterator = row.cellIterator();
//				cell = null;
//				while (cellIterator.hasNext()) {
//					++numberOfColumns;
//					cell = cellIterator.next();
//				}
//			}
//			int sheetIndex = 0;
//			for (String operatingSystem : operatingSystems) {
//				webDriver.findElement(By.xpath("//a[text()='" + operatingSystem + "']")).click();
//				for (String region : regions) {
//					System.out.println("working on " + operatingSystem + " @ " + region);
//					sheet = workbook.getSheetAt(sheetIndex++);
//					webElements = webDriver.findElements(By.xpath("//div[contains(@id, 'aws-element')][contains(@class, 'active')]//span[@class='dropdown-label']"));
//					for (WebElement webElement :webElements) {
//						if (webElement.isDisplayed()) {
//							webElement.click();
//							break;
//						}
//					}
//					webElements = webDriver.findElements(By.xpath("//div[@class='dropdown-list']//span[text()='" + region + "']"));
//					for (WebElement webElement :webElements) {
//						if (webElement.isDisplayed()) {
//							webElement.click();
//							break;
//						}
//					}
//					rowCounter = 0;
//					row = sheet.getRow(rowCounter++);
//					row.createCell(numberOfColumns).setCellValue(usage.get(operatingSystem) + " " + dateFormat.format(new Date()));
//					for (int instancesIndex = 0; instancesIndex < instancess.length; ++instancesIndex) {
//						for (int instanceIndex = 0; instanceIndex < instancess[instancesIndex].length; ++instanceIndex) {
//							try {
//								webDriver.findElement(By.xpath("//caption[text()='" + region + "']/..//td[text()='" + instancess[instancesIndex][instanceIndex] + "']/../td[@class='" + excelRow[0] + "']"));
//							} catch (NoSuchElementException e) {
//								continue;
//							}
//							row = sheet.getRow(rowCounter++);
//							row.createCell(numberOfColumns).setCellValue(webDriver.findElement(By.xpath("//caption[text()='" + region + "']/..//td[text()='" + instancess[instancesIndex][instanceIndex] + "']/../td[@class='" + rates.get(operatingSystem) + "']")).getText());
//						}
//					}
//				}
//			}
//			++numberOfColumns;
//		} else {
//			numberOfColumns = excelHeader.length;
//			workbook = new XSSFWorkbook();
//			for (String operatingSystem : operatingSystems) {
//				webDriver.findElement(By.xpath("//a[text()='" + operatingSystem + "']")).click();
//				for (String region : regions) {
//					System.out.println("working on " + operatingSystem + " @ " + region);
//					webElements = webDriver.findElements(By.xpath("//div[contains(@id, 'aws-element')][contains(@class, 'active')]//span[@class='dropdown-label']"));
//					for (WebElement webElement :webElements) {
//						if (webElement.isDisplayed()) {
//							webElement.click();
//							break;
//						}
//					}
//					webElements = webDriver.findElements(By.xpath("//div[@class='dropdown-list']//span[text()='" + region + "']"));
//					for (WebElement webElement :webElements) {
//						if (webElement.isDisplayed()) {
//							webElement.click();
//							break;
//						}
//					}
//
//					sheet = workbook.createSheet(operatingSystem + " @ " + region);
//					sheet.createFreezePane(1, 1);
//					rowCounter = 0;
//					row = sheet.createRow(rowCounter++);
//					for (int columnIndex = 0; columnIndex < excelHeader.length; ++columnIndex) {
//						if (excelHeader[columnIndex].equals("Usage")) {
//							row.createCell(columnIndex).setCellValue(usage.get(operatingSystem) + " " + dateFormat.format(new Date()));
//						} else {
//							row.createCell(columnIndex).setCellValue(excelHeader[columnIndex]);
//						}
//					}
//					for (int instancesIndex = 0; instancesIndex < instancess.length; ++instancesIndex) {
//						for (int instanceIndex = 0; instanceIndex < instancess[instancesIndex].length; ++instanceIndex) {
//							try {
//								webDriver.findElement(By.xpath("//caption[text()='" + region + "']/..//td[text()='" + instancess[instancesIndex][instanceIndex] + "']/../td[@class='" + excelRow[0] + "']"));
//							} catch (NoSuchElementException e) {
//								continue;
//							}
//							row = sheet.createRow(rowCounter++);
//							for (int columnIndex = 0; columnIndex < excelRow.length; ++columnIndex) {
//								if (excelRow[columnIndex].equals("type")) {
//									row.createCell(columnIndex).setCellValue(types[instancesIndex]);
//								} else if (excelRow[columnIndex].equals("rate")) {
//									row.createCell(columnIndex).setCellValue(webDriver.findElement(By.xpath("//caption[text()='" + region + "']/..//td[text()='" + instancess[instancesIndex][instanceIndex] + "']/../td[@class='" + rates.get(operatingSystem) + "']")).getText());
//								} else {
//									row.createCell(columnIndex).setCellValue(webDriver.findElement(By.xpath("//caption[text()='" + region + "']/..//td[text()='" + instancess[instancesIndex][instanceIndex] + "']/../td[@class='" + rates.get(operatingSystem) + "']/../td[@class='" + excelRow[columnIndex] + "']")).getText());
//									// row.createCell(columnIndex).setCellValue(webDriver.findElement(By.xpath("//caption[text()='" + region + "']/..//td[text()='" + instancess[instancesIndex][instanceIndex] + "']/../td[@class='" + excelRow[columnIndex] + "']")).getText());
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//		webDriver.quit();
//
//		// formatting
//		// sheet.autoSizeColumn(0);	// leave enough column width for drawing name
//		Font font = workbook.createFont();
//		font.setFontHeightInPoints((short) 12);
//		Font boldFont = workbook.createFont();
//		boldFont.setFontHeightInPoints((short) 12);
//		boldFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
//		cellStyle = workbook.createCellStyle();
//		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
//		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//		cellStyle.setFont(font);
//		cellStyle.setWrapText(true);
//		boldCellStyle = workbook.createCellStyle();
//		boldCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
//		boldCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//		boldCellStyle.setFont(boldFont);
//		boldCellStyle.setWrapText(true);
//		for (int sheetIndex = 0; sheetIndex < operatingSystems.length * regions.length; ++sheetIndex) {
//			sheet = workbook.getSheetAt(sheetIndex);
//			Iterator<Row> rowIterator = sheet.rowIterator();
//			while (rowIterator.hasNext()) {
//				Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
//				while (cellIterator.hasNext()) {
//					cell = cellIterator.next();
//					// bold the headers
//					if (cell.getRowIndex() <= 0 || cell.getColumnIndex() <= 0) {
//						cell.setCellStyle(boldCellStyle);
//					} else {
//						cell.setCellStyle(cellStyle);
//					}
//				}
//			}
//			for (int columnIndex = 0; columnIndex < numberOfColumns; ++columnIndex) {
//				sheet.autoSizeColumn(columnIndex);
//			}	// sheet.setColumnWidth(3, 36 * 256);
//		}
//		// try to write the excel file
//		try {
//			// create excel file
//			FileOutputStream fileOutputStream = new FileOutputStream(excelFile);
//			workbook.write(fileOutputStream);
//			fileOutputStream.close();
//			System.out.println("Excel file written successfully...");
//
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return numberOfColumns;
//	}
//
//	public void sleep(int seconds) {
//		try {
//			Thread.sleep(1000*seconds);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}

}
