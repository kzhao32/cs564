import java.io.IOException;
import java.sql.DriverManager;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExtractPiazzaDataWithP  {
	DecimalFormat decimalFormat = new DecimalFormat("##.00");
	public static void main(String[] args) {
		ExtractPiazzaDataWithP ExtractPiazzaDataInstance = new ExtractPiazzaDataWithP();
		ExtractPiazzaDataInstance.start();
	}

	public void start() {
		List<String> emails = new ArrayList<String>();
		List<String> instructorsAll = new ArrayList<String>();
		List<String> studentsAll = new ArrayList<String>();
		List<String> coursesAll = new ArrayList<String>();
		String PiazzaLink = "https://piazza.com/";
		String[] PiazzaEmail = { "Piazza Email: " };
		String[] PiazzaPassword = { "Piazza Password: " };
		int courseCounter = 0;
		int assignmentCounter = 0;
		int discussionCounter = 0;
		int issueCounter = 0;
		String[] seasons1 = { "Winter", "Spring", "Summer", "Fall" };
		String[] seasons2 = { "WI", "SP", "SU", "FA" };
		java.io.Console console = System.console();
		if (console != null) {
			PiazzaEmail[0] = console.readLine(PiazzaEmail[0]);
			PiazzaPassword[0] = new String(console.readPassword(PiazzaPassword[0]));
		} else {
			Scanner keyboard = new Scanner(System.in);
			//System.out.print(PiazzaLink);
			//System.out.println("PiazzaLink = \"" + PiazzaLink + "\"" + PiazzaLink.length());
			if (true) {
				String[] PiazzaEmail2 = { "kzhao32@wisc.edu", "tmdemuth@wisc.edu", "zhao68@uic.edu" };
				PiazzaEmail = PiazzaEmail2;
				String[] PiazzaPassword2 = { "123123qwer", "T2CP9JtqyccHU", "123123qwer" }; // TODO delete before committing
				PiazzaPassword = PiazzaPassword2;
			} else {
				System.out.print(PiazzaEmail);
				PiazzaEmail[0] = keyboard.nextLine();
				System.out.print(PiazzaPassword);
				PiazzaPassword[0] = keyboard.nextLine();
			}
			keyboard.close();
		}

		System.setProperty("webdriver.chrome.driver", "chromedriver");
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		}
		WebDriver webDriver = new ChromeDriver();
		
		System.out.println("CREATE TABLE person (email TEXT, password TEXT, firstName TEXT, lastName TEXT, PRIMARY KEY (email));");
		System.out.println("CREATE TABLE instructor (email TEXT, officeHourTime TEXT, officeHourLocation TEXT, PRIMARY KEY (email), FOREIGN KEY(email) REFERENCES person(email));");
		System.out.println("CREATE TABLE student (email TEXT, isGraduate INTEGER, PRIMARY KEY (email), FOREIGN KEY(email) REFERENCES person(email));");
		System.out.println("CREATE TABLE course (courseID INTEGER, courseName TEXT, year Integer, semester TEXT, PRIMARY KEY (courseID));");
		System.out.println("CREATE TABLE assignment (assignmentID INTEGER, courseID INTEGER, assignmentType TEXT, assignDate TEXT, dueDate TEXT, description TEXT, PRIMARY KEY (assignmentID), FOREIGN KEY(courseID) REFERENCES course(courseID));");
		System.out.println("CREATE TABLE discussion (discussionID INTEGER, assignmentID INTEGER, issueNumber INTEGER, time TEXT, summary TEXT, details TEXT, discussionType TEXT, studentResponse TEXT, instructorResponse TEXT, PRIMARY KEY (discussionID), FOREIGN KEY(assignmentID) REFERENCES assignment(assignmentID));");
		System.out.println("CREATE TABLE lectures (email TEXT, courseID INTEGER, FOREIGN KEY(email) REFERENCES instructor(email), FOREIGN KEY(courseID) REFERENCES course(courseID));");
		System.out.println("CREATE TABLE takes (email TEXT, courseID INTEGER, FOREIGN KEY(email) REFERENCES student(email), FOREIGN KEY(courseID) REFERENCES course(courseID));");
		System.out.println("CREATE TABLE worksOn (email TEXT, assignmentID INTEGER, grade REAL, FOREIGN KEY(email) REFERENCES student(email), FOREIGN KEY(assignmentID) REFERENCES assignment(assignmentID));");
		
		for (int PiazzaAccountIndex = 0; PiazzaAccountIndex < PiazzaEmail.length; ++PiazzaAccountIndex) {
			webDriver.get(PiazzaLink);
			sleep(4000);
			clickByXpathAndExpectNextXpath(webDriver, "//ul[@class='nav navbar-nav navbar-right']//button[text()='Login'][last()]", "//input[@id='email_field']");
			findElementByXpath(webDriver, "//input[@id='email_field']").sendKeys(PiazzaEmail[PiazzaAccountIndex]);
			findElementByXpath(webDriver, "//input[@id='password_field']").sendKeys(PiazzaPassword[PiazzaAccountIndex]);
			clickByXpathAndExpectNextXpath(webDriver, "//button[text()='Log in']", "//a[@id='network_button']");
			clickByXpath(webDriver, "//a[@id='network_button']");
			WebElement showInactiveClassWebElement = findElementByXpath(webDriver, "//a[text()='Show inactive classes']");
			if (showInactiveClassWebElement != null) {
				clickAndExpectNextXpath(webDriver, showInactiveClassWebElement, "//a[text()='Hide inactive classes']");
			}

			List<WebElement> courses = findElementsByXpath(webDriver, "//ul[@id='my_classes']/li");
			for (int courseIndex = 0; courseIndex < courses.size(); ++courseIndex) {
				List<String> instructorsInThisCourse = new ArrayList<String>();
				List<String> studentsInThisCourse = new ArrayList<String>();
				List<Integer> assignmentsInThisCourse = new ArrayList<Integer>();
				for (int tryIndex = 0; tryIndex < 6; ++tryIndex) {
					try {
						webDriver.get(PiazzaLink);
						sleep(4000);
						break;
					} catch (UnhandledAlertException e) {
						webDriver.switchTo().alert().accept();
					}
				}
				try {
					clickByXpath(webDriver, "//a[@id='network_button']");
					showInactiveClassWebElement = findElementByXpath(webDriver, "//a[text()='Show inactive classes']");
					if (showInactiveClassWebElement != null) {
						clickAndExpectNextXpath(webDriver, showInactiveClassWebElement, "//a[text()='Hide inactive classes']");
					}
					clickByXpath(webDriver, "//ul[@id='my_classes']/li[" + (courseIndex + 1) + "]");
					clickByXpath(webDriver, "//a[contains(text(), 'Resources')]");
					clickByXpath(webDriver, "//a[contains(text(), 'Staff')]");
					String[] courseData = findElementByXpath(webDriver, "//h1/../p").getText().split(" ");
					String courseYear = "NULL";
					String courseSemester = "NULL";
					for (String courseDatum : courseData) {
						if (courseDatum.length() == 4) {
							try {
								courseYear = "" + Integer.parseInt(courseDatum);
							} catch (NumberFormatException e) {
							}
						}
						for (int seasonIndex = 0; seasonIndex < seasons1.length; ++seasonIndex) {
							if (courseDatum.equalsIgnoreCase(seasons1[seasonIndex]) || courseDatum.equalsIgnoreCase(seasons2[seasonIndex])) {
								courseSemester = seasons1[seasonIndex];
							}
						}
					}
					String courseName = findElementByXpath(webDriver, "//h1").getText().replace("\"", "\\\"").replace("'", "''");
					if (coursesAll.contains(courseYear + courseSemester + courseName)) {
						continue;
					}
					System.out.println("INSERT INTO course VALUES(" + courseCounter + ", '" + courseName + "', " + courseYear + ", '" + courseSemester + "');");
					coursesAll.add(courseYear + courseSemester + courseName);
					List<WebElement> instructors = findElementsByXpath(webDriver, "//tbody[@class='instructors']/tr");
					for (WebElement instructor : instructors) {
						String fullName = findElementByXpath(instructor, "./td[2]").getText();
						String email = fullName.replaceAll("[^A-Za-z]+", "") + "@wisc.edu";
						String[] fullNameSplitted = fullName.split(" ");
						String firstName = fullNameSplitted[0].replaceAll("[^A-Za-z]+", "");
						String lastName = fullNameSplitted[fullNameSplitted.length - 1].replaceAll("[^A-Za-z]+", "");
						String officeHourTime = findElementByXpath(instructor, ".//Strong[text()='When?']/../..//td[@class='view']").getText();
						String officeHourLocation = findElementByXpath(instructor, ".//Strong[text()='Where?']/../..//td[@class='view']").getText();

						if (!emails.contains(email)) {
							emails.add(email);
							System.out.println("INSERT INTO person VALUES('" + email + "', 'password', '" + firstName + "', '" + lastName + "');");
						}
						if (!instructorsAll.contains(email)) {
							instructorsAll.add(email);
							System.out.println("INSERT INTO instructor VALUES('" + email + "', '" + officeHourTime + "', '" + officeHourLocation + "');");
						}
						if (!instructorsInThisCourse.contains(email)) {
							instructorsInThisCourse.add(email);
							System.out.println("INSERT INTO lectures VALUES('" + email + "', " + courseCounter + ");");
						}
					}
					clickByXpath(webDriver, "//a[contains(text(), 'Q & A')]");
					List<WebElement> assignments = findElementsByXpath(webDriver, "//a[@class='tag folder']");
					for (WebElement assignment : assignments) {
						String assignmentDescription = assignment.getText();
						String assignmentType = assignmentDescription.replaceAll("[^A-Za-z]+", "");
						if (assignmentType.contains("project")) {
							assignmentType = "project";
						}
						// System.out.println("assignmentDescription = " + assignmentDescription);
						// System.out.println("assignment.getText() = " + assignment.getText());
						clickAndExpectNextXpath(webDriver, assignment, "//strong[@id='filter_by'][text()=\"" + assignment.getText() + "\"]");

						String assignmentAssignDate = "";
						String assignmentDueDate = "";
						List<WebElement> discussions = findElementsByXpath(webDriver, "//div[@class='question_group']/ul/li");
						if (discussions.size() == 0) {
							continue;
						}
						try {
							//							System.out.println("clicking on \n" + "//div[@class='question_group'][1]/ul/li" + " \n and expecting \n" + "//h1[@class='post_region_title'][text()=\"" + findElementByXpath(discussions.get(0), ".//span[@class='title_text']").getText() + "\"]");
							clickByXpathAndExpectNextXpath(webDriver, "//div[@class='question_group'][1]/ul/li[1]", "//h1[@class='post_region_title'][text()=\"" + findElementByXpath(discussions.get(0), ".//span[@class='title_text']").getText() + "\"]");
							sleep(200);
							assignmentDueDate = findElementByXpath(webDriver, "//div[@class='post_region_actions_meta']//span[1]").getAttribute("title");
							//							System.out.println("clicking on \n" + "//div[@class='question_group'][" + discussions.size() + "]/ul/li" + " \n and expecting \n" + "//h1[@class='post_region_title'][text()=\"" + findElementByXpath(discussions.get(discussions.size() - 1), ".//span[@class='title_text']").getText() + "\"]");
							clickByXpathAndExpectNextXpath(webDriver, "//div[@class='question_group'][last()]/ul/li[last()]", "//h1[@class='post_region_title'][text()=\"" + findElementByXpath(discussions.get(discussions.size() - 1), ".//span[@class='title_text']").getText() + "\"]");
							sleep(200);
							assignmentAssignDate = findElementByXpath(webDriver, "//div[@class='post_region_actions_meta']//span[1]").getAttribute("title");
						} catch (NullPointerException | StaleElementReferenceException e) {
						}
						//					List<WebElement> assignmentAssignDateWebElement = findElementsByXpath(webDriver, "//div[@class='question_group'][last()]/ul/li[last()]//div[@class='timestamp']");
						//					assignmentAssignDate = assignmentAssignDateWebElement.size() == 0 ? "NULL" : assignmentAssignDateWebElement.get(0).getText();
						//					List<WebElement> assignmentDueDateWebElement = findElementsByXpath(webDriver, "//div[@class='question_group']/ul/li//div[@class='timestamp']");
						//					String assignmentDueDate = assignmentDueDateWebElement.size() == 0 ? "NULL" : assignmentDueDateWebElement.get(0).getText();
						assignmentsInThisCourse.add(assignmentCounter);
						System.out.println("INSERT INTO assignment VALUES(" + assignmentCounter + ", " + courseCounter + ", '" + assignmentType + "', '" + assignmentAssignDate + "', '" + assignmentDueDate + "', '" + assignmentDescription+ "');");

						List<WebElement> discussionGroups = findElementsByXpath(webDriver, "//div[@class='question_group']");
						for (int discussionGroupIndex = 0; discussionGroupIndex < discussionGroups.size(); ++discussionGroupIndex) {
							discussions = findElementsByXpath(webDriver, "//div[@class='question_group'][" + (discussionGroupIndex + 1) + "]/ul/li");
							for (int discussionIndex = 0; discussionIndex < discussions.size(); ++discussionIndex) {
								try {
									clickAndExpectNextXpath(webDriver, discussions.get(discussionIndex), "//h1[@class='post_region_title'][text()=\"" + findElementByXpath(discussions.get(discussionIndex), ".//span[@class='title_text']").getText() + "\"]");
									boolean stillLoading = false;
									for (int attemptIndex = 0; attemptIndex < 10; ++attemptIndex) {
										try {
											List<WebElement> users = findElementsByXpath(webDriver, "//span[contains(@class, 'user_name')]");
											for (int userIndex = 0; userIndex < users.size(); ++userIndex) {
												if (users.get(userIndex).getText().equals("Loading...")) {
													stillLoading = true;
													break;
												}
											}
											if (stillLoading) {
												sleep(500);
											} else {
												break;
											}
										} catch (NoSuchElementException | UnhandledAlertException e) {
										}
									}
									List<WebElement> users = findElementsByXpath(webDriver, "//span[contains(@class, 'user_name')]");
									for (int userIndex = 0; userIndex < users.size(); ++userIndex) {
										String fullName = users.get(userIndex).getText();
										String email = fullName.replaceAll("[^A-Za-z]+", "") + "@wisc.edu";
										if (fullName.length() == 0 || fullName.equals("Anonymous") || fullName.equals("Piazza Team") || instructorsInThisCourse.contains(email)) {
											continue;
										}
										String[] fullNameSplitted = fullName.split(" ");
										String firstName = fullNameSplitted[0].replaceAll("[^A-Za-z]+", "");
										String lastName = fullNameSplitted[fullNameSplitted.length - 1].replaceAll("[^A-Za-z]+", "");
										if (!emails.contains(email)) {
											emails.add(email);
											System.out.println("INSERT INTO person VALUES('" + email + "', 'password', '" + firstName + "', '" + lastName + "');");
										}
										if (!studentsAll.contains(email)) {
											studentsAll.add(email);
											System.out.println("INSERT INTO student VALUES('" + email + "', " + (int) (Math.random() * 2) + ");");
										}
										if (!studentsInThisCourse.contains(email)) {
											studentsInThisCourse.add(email);
											System.out.println("INSERT INTO takes VALUES('" + email + "', " + courseCounter + ");");
										}
									}

									// followup discussions
									String time = findElementByXpath(webDriver, "//div[@class='post_region_actions_meta']//span[1]").getAttribute("title");
									String discussionType = findElementByXpath(webDriver, "//div[@id='page_center']//div[@class='post_title']").getText();
									String discussionSummary = findElementByXpath(webDriver, "//h1[@class='post_region_title']").getText().replace("\"", "\\\"").replace("'", "''");
									String disucssionDetails = "";
									List<WebElement> lineWebElements = findElementsByXpath(webDriver, "//div[@id='questionText']/p");
									List<WebElement> lineWebElementsp;
									for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
										disucssionDetails += (disucssionDetails.length() == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\\\"").replace("'", "''");
									}
									String disucssionStudentResponse = "";
									lineWebElements = findElementsByXpath(webDriver, "//div[@id='s_answer']//p");
									for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
										disucssionStudentResponse += (disucssionStudentResponse.length() == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\\\"").replace("'", "''");
									}
									String disucssionInstructorResponse = "";
									lineWebElements = findElementsByXpath(webDriver, "//div[@id='instructor_answer']//p");
									for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
										disucssionInstructorResponse += (disucssionInstructorResponse.length() == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\\\"").replace("'", "''");
									}
									System.out.println("INSERT INTO discussion VALUES(" + discussionCounter + ", " + assignmentCounter + ", " + issueCounter + ", '" + time + "', '" + discussionSummary + "', '" + disucssionDetails + "', '" + discussionType + "', '" + disucssionStudentResponse + "', '" + disucssionInstructorResponse + "');");
									discussionCounter++;
									
									// followup discussions
									List<WebElement> followupDiscussions = findElementsByXpath(webDriver, "//div[@class='post_region_content clarifying_discussion']/div[not(contains(@id,'create_new_followup'))]");
									for (int followupDiscussionIndex = 0; followupDiscussionIndex < followupDiscussions.size(); ++followupDiscussionIndex) {
										time = findElementByXpath(followupDiscussions.get(followupDiscussionIndex), ".//a[@class='dicussion_meta']/span").getAttribute("title");
										discussionType = "followup question";
										disucssionDetails = "";
										lineWebElements = findElementsByXpath(followupDiscussions.get(followupDiscussionIndex), ".//span[@class='actual_text post_region_text']");
										for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
											disucssionDetails += (disucssionDetails.length() == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\\\"").replace("'", "''");
											lineWebElementsp = findElementsByXpath(lineWebElements.get(lineWebElementIndex), "./p");
											for (int lineWebElementpIndex = 0; lineWebElementpIndex < lineWebElementsp.size(); ++lineWebElementpIndex) {
												disucssionDetails += (disucssionDetails.length() == 0 ? "" : "\n") + lineWebElementsp.get(lineWebElementpIndex).getText().replace("\"", "\\\"").replace("'", "''");
											}
										}
										disucssionStudentResponse = "";
										disucssionInstructorResponse = "";
										lineWebElements = findElementsByXpath(followupDiscussions.get(followupDiscussionIndex), ".//span[@class='actual_reply_text post_region_text']");
										for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
											String replier = findElementByXpath(lineWebElements.get(lineWebElementIndex), "./../..//span[contains(@class,'user_name')]").getText();
											String email = replier.replaceAll("[^A-Za-z]+", "") + "@wisc.edu";
											if (instructorsInThisCourse.contains(email)) {
												disucssionInstructorResponse += (disucssionInstructorResponse.length() == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\\\"").replace("'", "''");
												lineWebElementsp = findElementsByXpath(lineWebElements.get(lineWebElementIndex), "./p");
												for (int lineWebElementpIndex = 0; lineWebElementpIndex < lineWebElementsp.size(); ++lineWebElementpIndex) {
													disucssionInstructorResponse += (disucssionInstructorResponse.length() == 0 ? "" : "\n") + lineWebElementsp.get(lineWebElementpIndex).getText().replace("\"", "\\\"").replace("'", "''");
												}
											} else {
												disucssionStudentResponse += (disucssionStudentResponse.length() == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\\\"").replace("'", "''");
												lineWebElementsp = findElementsByXpath(lineWebElements.get(lineWebElementIndex), "./p");
												for (int lineWebElementpIndex = 0; lineWebElementpIndex < lineWebElementsp.size(); ++lineWebElementpIndex) {
													disucssionStudentResponse += (disucssionStudentResponse.length() == 0 ? "" : "\n") + lineWebElementsp.get(lineWebElementpIndex).getText().replace("\"", "\\\"").replace("'", "''");
												}
											}
										}
										System.out.println("INSERT INTO discussion VALUES(" + discussionCounter + ", " + assignmentCounter + ", " + issueCounter + ", '" + time + "', '" + discussionSummary + "', '" + disucssionDetails + "', '" + discussionType + "', '" + disucssionStudentResponse + "', '" + disucssionInstructorResponse + "');");
										discussionCounter++;
									}
									issueCounter++;
								} catch (NullPointerException | StaleElementReferenceException e) {
								}
							}	// END discussionIndex
						}	// END discussionGroupIndex
						assignmentCounter++;
					}
					for (String studentInThisCourse : studentsInThisCourse) {
						for (int assignmentInThisCourse : assignmentsInThisCourse) {
							System.out.println("INSERT INTO worksOn VALUES('" + studentInThisCourse + "', " + assignmentInThisCourse + ", " + decimalFormat.format(Math.random() * 100) + ");");
						}
					}
					// Check the title of the page
					courseCounter++;
				} catch (NoSuchElementException | UnhandledAlertException e) {

				}
			}
			clickByXpathAndExpectNextXpath(webDriver, "//a[@id='user_button']", "//a[@id='log_out']");
			clickByXpathAndExpectNextXpath(webDriver, "//a[@id='log_out']", "//ul[@class='nav navbar-nav navbar-right']//button[text()='Login'][last()]");
		}

		System.out.println("Done. Page title is: " + webDriver.getTitle());

		//driver.quit();
	}

	public List<WebElement> findElements(WebDriver webDriver, By by) {
		List<WebElement> returnValue = null;
		for (int attemptIndex = 0; attemptIndex < 10; ++attemptIndex) {
			returnValue = webDriver.findElements(by);
			try {
				if (returnValue != null && (returnValue.size() == 0 || returnValue.get(0).isDisplayed())) {
					return returnValue;
				}
			} catch (org.openqa.selenium.StaleElementReferenceException e) {
			}
			sleep(500);
		}
		//System.out.println("failed findElements(WebDriver webDriver, By by) after 10 tries");
		return returnValue;
	}

	public List<WebElement> findElements(WebElement webElement, By by) {
		List<WebElement> returnValue = null;
		for (int attemptIndex = 0; attemptIndex < 10; ++attemptIndex) {
			returnValue = webElement.findElements(by);
			if (returnValue != null && (returnValue.size() == 0 || returnValue.get(0).isDisplayed())) {
				return returnValue;
			}
			sleep(500);
		}
		return returnValue;
	}

	//	public WebElement findElementById(WebDriver webDriver, String id) {
	//		List<WebElement> returnValue = findElements(webDriver, By.id(id));
	//		if (returnValue.size() > 0) {
	//			return returnValue.get(0);
	//		}
	//		return null;
	//	}

	public List<WebElement> findElementsByXpath(WebDriver webDriver, String xpath) {
		return findElements(webDriver, By.xpath(xpath));
	}

	public WebElement findElementByXpath(WebDriver webDriver, String xpath) {
		List<WebElement> returnValue = findElementsByXpath(webDriver, xpath);
		if (returnValue.size() > 0) {
			return returnValue.get(0);
		}
		return null;
	}

	public List<WebElement> findElementsByXpath(WebElement webElement, String xpath) {
		return findElements(webElement, By.xpath(xpath));
	}

	public WebElement findElementByXpath(WebElement webElement, String xpath) {
		return findElementsByXpath(webElement, xpath).get(0);
	}

	public void clickByXpath(WebDriver webDriver, String xpath) {
		for (int attemptIndex = 0; attemptIndex < 6; ++attemptIndex) {
			WebElement webElement = webDriver.findElement(By.xpath(xpath));
			try {
				webElement.click();
				sleep(500);
				return;
			} catch (NullPointerException | org.openqa.selenium.WebDriverException e) {
			}
			sleep(500);
		}
		return;
		//webDriver.findElement(By.xpath(xpath)).click();
	}

	public void clickByXpathAndExpectNextXpath(WebDriver webDriver, String xpath, String xpath2) {
		for (int attemptIndex = 0; attemptIndex < 6; ++attemptIndex) {
			WebElement webElement = webDriver.findElement(By.xpath(xpath));
			try {
				webElement.click();
				sleep(500);
				if (webDriver.findElement(By.xpath(xpath2)) != null) {
					return;
				}
			} catch (NullPointerException | org.openqa.selenium.WebDriverException e) {
			}
			sleep(500);
		}
		return;
		//webDriver.findElement(By.xpath(xpath)).click();
	}

	public void clickAndExpectNextXpath(WebDriver webDriver, WebElement webElement, String xpath) {
		for (int attemptIndex = 0; attemptIndex < 10; ++attemptIndex) {
			try {
				webElement.click();
				sleep(500);
				if (webDriver.findElement(By.xpath(xpath)) != null) {
					return;
				}
				return;
			} catch (org.openqa.selenium.WebDriverException e) {
			}
			sleep(500);
		}
		//		System.out.println("failed click(WebElement webElement) after 10 tries");		
		//		sleep(2);
	}

	public void sleep(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}