import java.io.Console;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class RegisterForCourses {
	
	WebDriver d1;
	String netID = "";
	String password = "";
	String term = "Fall 2014";
	String[] subjectIDs = { "ECE", "CS" };
	String[] CRNs = { "10647", "37161", "11122", "11130" };

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		RegisterForCourses instanceOfRegisterForCourses = new RegisterForCourses();
		instanceOfRegisterForCourses.start();
	}
	
	public void start() {
		getUserInputs(); 
		d1 = new FirefoxDriver();
		d1.get("https://my.uic.edu/common/");
		click("//span[text()='login']");
		sendKeys("//input[@id='UserIDinput']", netID);
		click("//input[@value='Enter']");
		sendKeys("//input[@id='passwordInput']", password);
		click("//input[@value='Enter']");
		click("//span[text()='Academics']");
		String oldTab = d1.getWindowHandle();
		click("//a[text()='Student Self Service Login - Registration and Records Menu']");
		ArrayList<String> newTab = new ArrayList<String>(d1.getWindowHandles());
		newTab.remove(oldTab);
		d1.close();
		d1.switchTo().window(newTab.get(0));
		sleep(5);
		click("//a[text()='Registration & Records']");
		click("//span[text()='Registration']");
		click("//a[text()='Look-up or Select Classes']");
		click("//a[text()='I Agree to the Above Statement']");
		sendKeysByID("term_input_id", term);
		click("//input[@value='Submit']");
		click("//input[@value='Advanced Search']");
		Select subject = selectById("subj_id");
		for (String s : subjectIDs) {
			subject.selectByValue(s);
		}
		click("//input[@value='Section Search']");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		while (true) {
			boolean canRegisterAllCRNs = true;
			System.out.print("time: " + dateFormat.format(new Date()));
			for (String crn : CRNs) {
				String sectionRemaining = getText("//a[text()='" + crn + "']/../../td[13]");
				System.out.print("; " + crn + "'s Remaining: " + sectionRemaining);
				if (sectionRemaining.equals("0")) {
					canRegisterAllCRNs = false;
				} else {
					// TODO select the checkbox
				}
			}
			System.out.println();
			if (canRegisterAllCRNs) {
				// TODO click on register at the bottom
				break;
			}
			d1.navigate().refresh();
			d1.switchTo().alert().accept();
			// wait about 30 seconds before refreshing...
			sleep((int)(5 + Math.random() * 10));
		}
		System.out.println("registered for courses on " + dateFormat.format(new Date()));
	}
		
	public void getUserInputs() {
		Console cnsl = null;
		try{
			// creates a console object
			cnsl = System.console();
			String netIDPrompt = "NetID: ";
			String passwordPrompt = "Password: ";
			String termPrompt = "term (Season Year) (e.g. \"Fall 2014\", or \"Summer 2015\": ";
			String sejectIDsPriompt = "subjectIDs (e.g. \"CS, ECE\"): ";
			String crnsPrompt = "CRNs (e.g. \"10647, 37161, 11122, 11130\"): ";
			// if console is not null
			if (cnsl != null) {
				netID = cnsl.readLine(netIDPrompt);
				password = new String(cnsl.readPassword(passwordPrompt));
				term = cnsl.readLine(termPrompt);
				if (term.equals("default")) {
					term = "Fall 2014";
					return;
				}
				subjectIDs = cnsl.readLine(sejectIDsPriompt).toUpperCase().replace(" ", "").split(",");
				CRNs = cnsl.readLine(crnsPrompt).replace(" ", "").split(",");
			} else {
				Scanner keyboard = new Scanner(System.in);
				System.out.print(netIDPrompt);
				netID = keyboard.nextLine();
				System.out.print(passwordPrompt);
				password = keyboard.nextLine();
				System.out.print(termPrompt);
				term = keyboard.nextLine();
				if (term.equals("default")) {
					term = "Fall 2014";
					keyboard.close();
					return;
				}
				System.out.print(sejectIDsPriompt);
				subjectIDs = keyboard.nextLine().toUpperCase().replace(" ", "").split(",");
				System.out.print(crnsPrompt);
				CRNs = keyboard.nextLine().replace(" ", "").split(",");
				keyboard.close();
			}
		} catch(Exception ex){
			// if any error occurs
			ex.printStackTrace();      
		}
	}
	
	public void sleep(int seconds) {
		try {
			Thread.sleep(1000 * seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// ** Browser Access Tier **********************************************************************
	public void click(String xpath) {
		d1.findElement(By.xpath(xpath)).click();
		sleep(2);
	}
	
	public void sendKeys(String xpath, String keys) {
		d1.findElement(By.xpath(xpath)).sendKeys(keys);
		sleep(2);
	}
	
	public void sendKeysByID(String id, String keys) {
		d1.findElement(By.id(id)).sendKeys(keys);
		sleep(2);
	}
	
	public Select selectById(String id) {
		return new Select(d1.findElement(By.id(id)));
	}
	
	public String getText(String xpath) {
		return d1.findElement(By.xpath(xpath)).getText();
	}
	// ** End Browser Access Tier ******************************************************************
}
