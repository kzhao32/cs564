import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExtractPiazzaDataDoubleQuote  {
	public static void main(String[] args) {
		ExtractPiazzaDataDoubleQuote ExtractPiazzaDataInstance = new ExtractPiazzaDataDoubleQuote();
		ExtractPiazzaDataInstance.start();
	}

	public void start() {
		List<String> emails = new ArrayList<String>();
		String PiazzaLink = "https://piazza.com/";
		String PiazzaEmail = "Piazza Email: ";
		String PiazzaPassword = "Piazza Password: ";
		int courseCounter = 0;
		int assignmentCounter = 0;
		int discussionCounter = 0;
		int issueCounter = 0;
		String[] seasons1 = { "Winter", "Spring", "Summer", "Fall" };
		String[] seasons2 = { "WI", "SP", "SU", "FA" };
		java.io.Console console = System.console();
		if (console != null) {
			PiazzaEmail = console.readLine(PiazzaEmail);
			PiazzaPassword = new String(console.readPassword(PiazzaPassword));
		} else {
			Scanner keyboard = new Scanner(System.in);
			//System.out.print(PiazzaLink);
			//System.out.println("PiazzaLink = \"" + PiazzaLink + "\"" + PiazzaLink.length());
			if (true) {
				PiazzaEmail = "kzhao32@wisc.edu";
				PiazzaPassword = "123123qwer"; // TODO delete before committing
			} else {
				System.out.print(PiazzaEmail);
				PiazzaEmail = keyboard.nextLine();
				System.out.print(PiazzaPassword);
				PiazzaPassword = keyboard.nextLine();
			}
			keyboard.close();
		}
		System.setProperty("webdriver.chrome.driver", "chromedriver");
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		}
		WebDriver webDriver = new ChromeDriver();
		webDriver.get(PiazzaLink);
		sleep(5);
		clickByXpath(webDriver, "//ul[@class='nav navbar-nav navbar-right']//button[text()='Login'][last()]");
		findElementById(webDriver, "email_field").sendKeys(PiazzaEmail);
		findElementById(webDriver, "password_field").sendKeys(PiazzaPassword);
		clickByXpath(webDriver, "//button[text()='Log in']");
		clickByXpath(webDriver, "//a[@id='network_button']");
		WebElement showInactiveClassWebElement = findElementByXpath(webDriver, "//a[text()='Show inactive classes']");
		if (showInactiveClassWebElement != null) {
			click(showInactiveClassWebElement);
		}
		System.out.println("CREATE TABLE person (email TEXT, password TEXT, firstName TEXT, lastName TEXT, PRIMARY KEY (email));");
		System.out.println("CREATE TABLE instructor (email TEXT, officeHourTime TEXT, officeHourLocation TEXT, PRIMARY KEY (email), FOREIGN KEY(email) REFERENCES person(email));");
		System.out.println("CREATE TABLE student (email TEXT, isGraduate INTEGER, PRIMARY KEY (email), FOREIGN KEY(email) REFERENCES person(email));");
		System.out.println("CREATE TABLE course (courseID INTEGER, courseName TEXT, year Integer, semester TEXT, PRIMARY KEY (courseID));");
		System.out.println("CREATE TABLE assignment (assignmentID INTEGER, courseID INTEGER, assignmentType TEXT, assignDate TEXT, dueDate TEXT, description TEXT, PRIMARY KEY (assignmentID), FOREIGN KEY(courseID) REFERENCES course(courseID));");
		System.out.println("CREATE TABLE discussion (discussionID INTEGER, assignmentID INTEGER, issueNumber INTEGER, time TEXT, summary TEXT, details TEXT, discussionType TEXT, studentResponse TEXT, instructorResponse TEXT, PRIMARY KEY (discussionID), FOREIGN KEY(assignmentID) REFERENCES assignment(assignmentID));");
		System.out.println("CREATE TABLE lectures (email TEXT, courseID INTEGER, FOREIGN KEY(email) REFERENCES instructor(email), FOREIGN KEY(courseID) REFERENCES course(courseID));");
		System.out.println("CREATE TABLE takes (email TEXT, courseID INTEGER, FOREIGN KEY(email) REFERENCES student(email), FOREIGN KEY(courseID) REFERENCES course(courseID));");
		System.out.println("CREATE TABLE worksOn (email TEXT, assignmentID INTEGER, grade REAL, FOREIGN KEY(email) REFERENCES student(email), FOREIGN KEY(assignmentID) REFERENCES assignment(assignmentID));");
		List<WebElement> courses = findElementsByXpath(webDriver, "//ul[@id='my_classes']/li");
		for (WebElement course : courses) {
			int issueNumber = 0;
			List<String> studentsInThisCourse = new ArrayList<String>();
			List<Integer> assignmentsInThisCourse = new ArrayList<Integer>();
			webDriver.get(PiazzaLink);
			sleep(5);
			try {
				clickByXpath(webDriver, "//a[@id='network_button']");
				showInactiveClassWebElement = findElementByXpath(webDriver, "//a[text()='Show inactive classes']");
				if (showInactiveClassWebElement != null) {
					click(showInactiveClassWebElement);
				}
				clickByXpath(webDriver, "//ul[@id='my_classes']/li[" + (courseCounter + 1) + "]");
				clickByXpath(webDriver, "//a[contains(text(), 'Resources')]");
				clickByXpath(webDriver, "//a[contains(text(), 'Staff')]");
				String[] courseData = findElementByXpath(webDriver, "//h1/../p").getText().split(" ");
				String courseYear = "NULL";
				String courseSemester = "NULL";
				for (String courseDatum : courseData) {
					if (courseDatum.length() == 4) {
						try {
							courseYear = "" + Integer.parseInt(courseDatum);
						} catch (NumberFormatException e) {
						}
					}
					for (int seasonIndex = 0; seasonIndex < seasons1.length; ++seasonIndex) {
						if (courseDatum.equalsIgnoreCase(seasons1[seasonIndex]) || courseDatum.equalsIgnoreCase(seasons2[seasonIndex])) {
							courseSemester = seasons1[seasonIndex];
						}
					}
				}
				System.out.println("INSERT INTO \"course\" VALUES(" + courseCounter + ", \"" + findElementByXpath(webDriver, "//h1").getText() + "\", " + courseYear + ", \"" + courseSemester + "\");");
				List<WebElement> instructors = findElementsByXpath(webDriver, "//tbody[@class='instructors']/tr");
				for (WebElement instructor : instructors) {
					String fullName = findElementByXpath(instructor, "./td[2]").getText();
					String email = fullName.replaceAll("[^A-Za-z]+", "") + "@wisc.edu";
					String[] fullNameSplitted = fullName.split(" ");
					String firstName = fullNameSplitted[0];
					String lastName = fullNameSplitted[fullNameSplitted.length - 1];
					String officeHourTime = findElementByXpath(webDriver, ".//Strong[text()='When?']/../..//td[@class='view']").getText();
					String officeHourLocation = findElementByXpath(webDriver, ".//Strong[text()='Where?']/../..//td[@class='view']").getText();
					if (!emails.contains(email)) {
						emails.add(email);
						System.out.println("INSERT INTO \"person\" VALUES(\"" + email + "\", \"password\", \"" + firstName + "\", \"" + lastName + "\");");
						System.out.println("INSERT INTO \"instructor\" VALUES(\"" + email + "\", \"" + officeHourTime + "\", \"" + officeHourLocation + "\");");
					}
					System.out.println("INSERT INTO \"lectures\" VALUES(\"" + email + "\", " + courseCounter + ", \"" + firstName + "\", \"" + lastName + "\");");
				}
				clickByXpath(webDriver, "//a[contains(text(), 'Q & A')]");
				List<WebElement> assignments = findElementsByXpath(webDriver, "//a[@class='tag folder']");
				for (WebElement assignment : assignments) {
					String assignmentDescription = assignment.getText();
					String assignmentType = assignmentDescription.replaceAll("[^A-Za-z]+", "");
					if (assignmentType.contains("project")) {
						assignmentType = "project";
					}
					//				System.out.println("assignmentDescription = " + assignmentDescription);
					click(assignment);
					String assignmentAssignDate = "NULL";
					List<WebElement> assignmentAssignDateWebElement = findElementsByXpath(webDriver, "//div[@class='question_group'][last()]/ul/li[last()]//div[@class='timestamp']");
					assignmentAssignDate = assignmentAssignDateWebElement.size() == 0 ? "NULL" : assignmentAssignDateWebElement.get(0).getText();
					List<WebElement> assignmentDueDateWebElement = findElementsByXpath(webDriver, "//div[@class='question_group']/ul/li//div[@class='timestamp']");
					String assignmentDueDate = assignmentDueDateWebElement.size() == 0 ? "NULL" : assignmentDueDateWebElement.get(0).getText();
					assignmentsInThisCourse.add(assignmentCounter);
					System.out.println("INSERT INTO \"assignment\" VALUES(" + assignmentCounter + ", " + courseCounter + ", \"" + assignmentType + "\", \"" + assignmentAssignDate + "\", \"" + assignmentDueDate + "\", \"" + assignmentDescription+ "\");");

					List<WebElement> discussionGroups = findElementsByXpath(webDriver, "//div[@class='question_group']");
					for (int discussionGroupIndex = 0; discussionGroupIndex < discussionGroups.size(); ++discussionGroupIndex) {
						List<WebElement> discussions = findElementsByXpath(webDriver, "//div[@class='question_group'][" + (discussionGroupIndex + 1) + "]/ul/li");
						for (int discussionIndex = 0; discussionIndex < discussions.size(); ++discussionIndex) {
							try {
								click(discussions.get(discussionIndex));
								String time = findElementByXpath(webDriver, "//div[@class='question_group'][" + (discussionGroupIndex + 1) + "]/ul/li[" + (discussionIndex + 1) + "]//div[@class='timestamp']").getText();
								String discussionType = findElementByXpath(webDriver, "//div[@id='page_center']//div[@class='post_title']").getText();
								String discussionSummary = findElementByXpath(webDriver, "//h1[@class='post_region_title']").getText();
								String disucssionDetails = "";
								List<WebElement> lineWebElements = findElementsByXpath(webDriver, "//div[@id='questionText']/p");
								for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
									disucssionDetails += (lineWebElementIndex == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\"\"");
								}
								String disucssionStudentResponse = "";
								lineWebElements = findElementsByXpath(webDriver, "//div[@id='s_answer']//p");
								for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
									disucssionStudentResponse += (lineWebElementIndex == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\"\"");
								}
								String disucssionInstructorResponse = "";
								lineWebElements = findElementsByXpath(webDriver, "//div[@id='instructor_answer']//p");
								for (int lineWebElementIndex = 0; lineWebElementIndex < lineWebElements.size(); ++lineWebElementIndex) {
									disucssionInstructorResponse += (lineWebElementIndex == 0 ? "" : "\n") + lineWebElements.get(lineWebElementIndex).getText().replace("\"", "\"\"");
								}
								System.out.println("INSERT INTO \"discussion\" VALUES(" + discussionCounter + ", " + assignmentCounter + ", " + issueNumber + ", \"" + time + "\", \"" + discussionSummary + "\", \"" + disucssionDetails + "\", \"" + discussionType+ "\", \"" + disucssionStudentResponse + "\", \"" + disucssionInstructorResponse + "\");");
								
								List<WebElement> users = findElementsByXpath(webDriver, "//span[contains(@class, 'user_name')]");
								for (int userIndex = 0; userIndex < users.size(); ++userIndex) {
									String fullName = users.get(userIndex).getText();
									String email = fullName.replaceAll("[^A-Za-z]+", "") + "@wisc.edu";
									String[] fullNameSplitted = fullName.split(" ");
									String firstName = fullNameSplitted[0];
									String lastName = fullNameSplitted[fullNameSplitted.length - 1];
									if (!emails.contains(email)) {
										emails.add(email);
										studentsInThisCourse.add(email);
										System.out.println("INSERT INTO \"person\" VALUES(\"" + email + "\", \"password\", \"" + firstName + "\", \"" + lastName + "\");");
										System.out.println("INSERT INTO \"student\" VALUES(\"" + email + "\", " + (int) (Math.random() * 2) + ");");
										System.out.println("INSERT INTO \"takes\" VALUES(\"" + email + "\", " + courseCounter + ");");
									}
									if (!studentsInThisCourse.contains(email)) {
										studentsInThisCourse.add(email);
										System.out.println("INSERT INTO \"takes\" VALUES(\"" + email + "\", " + courseCounter + ");");
									}
								}

							} catch (NullPointerException | StaleElementReferenceException e) {
							}
							discussionCounter++;
						}
						issueNumber++;
					}
					assignmentCounter++;
				}
				for (String studentInThisCourse : studentsInThisCourse) {
					for (int assignmentInThisCourse : assignmentsInThisCourse) {
						System.out.println("INSERT INTO \"worksOn\" VALUES(\"" + studentInThisCourse + "\", " + assignmentInThisCourse + ", " + (Math.random() * 100) + ");");
					}
				}
				
				// Check the title of the page
				courseCounter++;
			} catch (NoSuchElementException | UnhandledAlertException e) {

			}
		}
		
		System.out.println("Done. Page title is: " + webDriver.getTitle());

		//driver.quit();
	}
	
	public List<WebElement> findElements(WebDriver webDriver, By by) {
		List<WebElement> returnValue = null;
		for (int attemptIndex = 0; attemptIndex < 10; ++attemptIndex) {
			returnValue = webDriver.findElements(by);
			try {
				if (returnValue != null && (returnValue.size() == 0 || returnValue.get(0).isDisplayed())) {
					return returnValue;
				}
			} catch (org.openqa.selenium.StaleElementReferenceException e) {
			}
			sleep(500);
		}
		//System.out.println("failed findElements(WebDriver webDriver, By by) after 10 tries");
		return returnValue;
	}
	
	public List<WebElement> findElements(WebElement webElement, By by) {
		List<WebElement> returnValue = null;
		for (int attemptIndex = 0; attemptIndex < 10; ++attemptIndex) {
			returnValue = webElement.findElements(by);
			if (returnValue != null && returnValue.get(0).isDisplayed()) {
				return returnValue;
			}
			sleep(500);
		}
		return returnValue;
	}
	
	public WebElement findElementById(WebDriver webDriver, String id) {
		List<WebElement> returnValue = findElements(webDriver, By.id(id));
		if (returnValue.size() > 0) {
			return returnValue.get(0);
		}
		return null;
	}
	
	public List<WebElement> findElementsByXpath(WebDriver webDriver, String xpath) {
		return findElements(webDriver, By.xpath(xpath));
	}
	
	public WebElement findElementByXpath(WebDriver webDriver, String xpath) {
		List<WebElement> returnValue = findElementsByXpath(webDriver, xpath);
		if (returnValue.size() > 0) {
			return returnValue.get(0);
		}
		return null;
	}
	
	public WebElement findElementByXpath(WebElement webElement, String xpath) {
		return findElements(webElement, By.xpath(xpath)).get(0);
	}
	
	public void clickByXpath(WebDriver webDriver, String xpath) {
		for (int attemptIndex = 0; attemptIndex < 6; ++attemptIndex) {
			WebElement webElement = webDriver.findElement(By.xpath(xpath));
			try {
				webElement.click();
				sleep(500);
				return;
			} catch (NullPointerException | org.openqa.selenium.WebDriverException e) {
			}
			sleep(500);
		}
		return;
		//webDriver.findElement(By.xpath(xpath)).click();
//		sleep(2); // TODO lessen delay. instead, just add retries for find
	}
	
	public void click(WebElement webElement) {
		for (int attemptIndex = 0; attemptIndex < 10; ++attemptIndex) {
			try {
				webElement.click();
				sleep(500);
				return;
			} catch (org.openqa.selenium.WebDriverException e) {
			}
			sleep(500);
		}
		System.out.println("failed click(WebElement webElement) after 10 tries");		
//		sleep(2);
	}

	public void sleep(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}