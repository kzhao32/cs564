function testext() {
	alert("Working");
}

function login(email, password) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=login&email="+email+"&password="+password);

	return xhr.responseText;
}

function createInstructorAccount(email, password, firstName, lastName, officeHourTime, officeHourLocation) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=createInstructorAccount&email="+email+"&password="+password+"&firstName="+firstName+"&lastName="+lastName+"&officeHourTime="+officeHourTime+"&officeHourLocation="+officeHourLocation);

	return xhr.responseText;
}

function createStudentAccount(email, password, firstName, lastName, isGraduate) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=createStudentAccount&email="+email+"&password="+password+"&firstName="+firstName+"&lastName="+lastName+"&isGraduate="+isGraduate);

	return xhr.responseText;
}

function createCourse(Name, Year, Semester) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=createCourse&name="+Name+"&year="+Year+"&semester="+Semester);

	return xhr.responseText;
}

function createAssignment(courseID, type, assignDate, dueDate, description) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=createAssignment&courseID="+courseID+"&type="+type+"&assignDate="+assignDate+"&dueDate="+dueDate+"&description="+description);

	return xhr.responseText;
}

function createDiscussion(assignmentID, issueNumber, time, type) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=createDiscussion&assignmentID="+assignmentID+"&issueNumber="+issueNumber+"&time="+time+"&type="+type);

	return xhr.responseText;
}

function addLectures(instructorEmail, courseID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addLectures&instructorEmail="+instructorEmail+"&courseID="+courseID);

	return xhr.responseText;
}

function addTakes(studentName, courseID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addTakes&studentName="+studentName+"&courseID="+courseID);

	return xhr.responseText;
}

function addWorksOn(studentEmail, assignmentID, grade) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addWorksOn&studentEmail="+studentEmail+"&assignmentID="+assignmentID+"&grade="+grade);

	return xhr.responseText;
}

function updateInstructorAccount(email, password, firstName, lastName, officeHourTime, officeHourLocation) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=updateInstructorAccount&email="+email+"&password="+password+"&firstName="+firstName+"&lastName="+lastName+"&officeHourTime="+officeHourTime+"&officeHourLocation="+officeHourLocation);

	return xhr.responseText;
}

function updateStudentAccount(email, password, firstName, lastName, isGraduate) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=updateStudentAccount&email="+email+"&password="+password+"&firstName="+firstName+"&lastName="+lastName+"&isGraduate="+isGraduate);

	return xhr.responseText;
}

function updateCourse(courseID, Name, Year, Semester) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=updateCourse&courseID="+courseID+"&name="+Name+"&year="+Year+"&semester="+Semester);

	return xhr.responseText;
}

function updateAssignment(assignmentID, courseID, type, assignDate, dueDate, description) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=updateAssignment&assignmentID="+assignmentID+"&courseID="+courseID+"&type="+type+"&assignDate="+assignDate+"&dueDate="+dueDate+"&description="+description);

	return xhr.responseText;
}

function updateDiscussion(discussionID, assignmentID, issueNumber, time, type, studentResponse, instructorResponse) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=updateDiscussion&discussionID="+discussionID+"&assignmentID="+assignmentID+"&issueNumber="+issueNumber+"&time="+time+"&type="+type+"&studentResponse="+studentResponse+"&instructorResponse="+instructorResponse);

	return xhr.responseText;
}

function updateWorksOn(studentEmail, assignmentID, grade) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=updateWorksOn&studentEmail="+studentEmail+"&assignmentID="+assignmentID+"&grade="+grade);

	return xhr.responseText;
}

function deleteAccount(email, password, type) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=deleteAccount&email="+email+"&password="+password+"&type="+type);

	return xhr.responseText;
}

function deleteCourse(courseID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=deleteCourse&courseID="+courseID);

	return xhr.responseText;
}

function deleteAssignment(assignmentID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=deleteAssignment&assignmentID="+assignmentID);
}

function deleteDiscussion(discussionID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=deleteDiscussion&discussionID="+discussionID);

	return xhr.responseText;
}

function deleteLectures(instructorEmail, courseID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=deleteLectures&instructorEmail="+instructorEmail+"&courseID="+courseID);

	return xhr.responseText;
}

function deleteTakes(studentName, courseID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=deleteTakes&studentName="+studentName+"&courseID="+courseID);

	return xhr.responseText;
}

function deleteWorksOn(studentEmail, assignmentID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=deleteWorksOn&studentEmail="+studentEmail+"&assignmentID="+assignmentID);

	return xhr.responseText;
}

function getInstructorAccount() {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getInstructorAccount");

	return xhr.responseText;
}

function getStudentAccount() {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getStudentAccount");

	return xhr.responseText;
}

function getCourse(courseID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getCourse&courseID="+courseID);

	return xhr.responseText;
}

function getAssignment(assignmentID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getAssignment&assignmentID="+assignmentID);

	return xhr.responseText;
}

function getDiscussion(discussionID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getDiscussion&discussionID="+discussionID);

	return xhr.responseText;
}

function getGrades() {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getGrades");

	return xhr.responseText;
}

function assignGrade(email, year, semester, courseName, description, grade) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=assignGrade&email="+email+"&year="+year+"&semester="+semester+"&courseName="+courseName+"&description="+description+"&grade="+grade);

	return xhr.responseText;
}

function addCourse(year, semester, courseName) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addCourse&year="+year+"&semester="+semester+"&courseName="+courseName);

	return xhr.responseText;
}

function enrollCourse(year, semester, courseName) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=enrollCourse&year="+year+"&semester="+semester+"&courseName="+courseName);

	return xhr.responseText;
}

function addAssignment(courseID, type, assignDate, dueDate, description) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addAssignment&courseID="+courseID+"&type="+type+"&assignDate="+assignDate+"&dueDate="+dueDate+"&description="+description);

	return xhr.responseText;
}

function addNewDiscussion(assignmentID, time, type, summary, details) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addNewDiscussion&assignmentID="+assignmentID+"&time="+time+"&type="+type+"&summary="+summary+"&details="+details);

	return xhr.responseText;
}

function addFollowupDiscussion(issueNumber, time, details) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addFollowupDiscussion&issueNumber="+issueNumber+"&time="+time+"&details="+details);

	return xhr.responseText;
}

function addResponse(discussionID, text) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=addResponse&discussionID="+discussionID+"&text="+text);

	return xhr.responseText;
}

function getCourses() {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getCourses");

	return xhr.responseText;
}

function getAssignments(courseID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getAssignments&courseID="+courseID);

	return xhr.responseText;
}

function getDiscussions(assignmentID) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getDiscussions&assignmentID="+assignmentID);

	return xhr.responseText;
}

function getDiscussionDetails(issueNumber) {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getDiscussionDetails&issueNumber="+issueNumber);

	return xhr.responseText;
}

function getCourseName(course) {
	var courseName1 = course.split("=");
	var courseName2;
	if(courseName1.length > 1) {
		courseName2 = courseName1[1].split(":");
		return courseName2[0];
	}
	return "ERROR";
}

function getCourseId(course) {
	var courseId = course.split("=");
	if(courseId.length > 1){
		return courseId[1];
	} else {
		return "ERROR";
	}
}

function getAccountType() {
	var xhr = new XMLHttpRequest();

	xhr.open("POST", "cgi-bin/FooBoard.cgi", false);
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xhr.send("method=getAccountType");

	return xhr.responseText;
}

function getAllMyCourses() {
	var getAllCourses = getCourses();
	if(getAllCourses == "") {
		coursesText = "<form id =\"class_list\" class=\"class_list\"><table><tr>";
		coursesText += "<td>";
		coursesText += "You have no courses."
		coursesText += "</td>";
		coursesText += "<td>";
		coursesText += "<button type=\"button\" value=\"add_class\" id=\"enrollClassBtn\" name=\"class2\" onclick=\"location.href='enroll_class.html'\">Enroll In Class</button>";
		coursesText += "</td>";
		coursesText += "</tr></table></form>";
		document.getElementById("lists").innerHTML = coursesText;
		return -1;
	} else {
		var allCourses = getAllCourses.split("&");
		var coursesText = "<form id=\"class_list\" class=\"class_list\"><table><tr>";
		var courseID = [];
		for(var i = 0; i < allCourses.length; i += 2) {
			coursesText += "<td>";
			coursesText += "<button type=\"botton\" value=\""+ getCourseId(allCourses[i])+"\" id=\"c_botton" + i / 2 + "\" name=\"course\">" + getCourseName(allCourses[i+1]) + "</button>";
			coursesText += "</td>";
			courseID[i / 2] = getCourseId(allCourses[i]);
		}
		var accountType = getAccountType();
		if(accountType == "instructor") {
			coursesText += "<td><button type=\"button\" value=\"add_class\" id=\"enrollClassBtn\" name=\"class2\" onclick=\"location.href='add_class.html'\">Add New Class</button></td>";
			coursesText += "<div id=\"grads_check\"><button type=\"button\" value=\"main\" id=\"mainBtn\" name=\"main\" onclick=\"location.href='main_instructor.html'\">Main Page</button></div>";
		} else if(accountType == "student") {
			coursesText += "<td><button type=\"button\" value=\"add_class\" id=\"enrollClassBtn\" name=\"class2\" onclick=\"location.href='enroll_class.html'\">Enroll In Class</button></td>";
			coursesText += "<div id=\"grads_check\"><button type=\"button\" value=\"main\" id=\"mainBtn\" name=\"main\" onclick=\"location.href='main_student.html'\">Main Page</button></div>";
		}
		coursesText += "</tr></table></form>";
		document.getElementById("lists").innerHTML = coursesText;
		return courseID;
	}
}

function getAllMyAssignments(courseIDs) {
	var getAllAssignments = getAssignments(courseIDs);
	if(getAllAssignments == "") {
		var assignText = "<form id=\"topic_list\" class=\"topic_list\">";
		assignText += "<input type=\"hidden\" name=\"class\" value=\"" + courseIDs + "\">";
		assignText += "<table><tr><td>No assignments for this course.</td></tr></table>";
		assignText += "</form>";
		document.getElementById("lists_of_topics").innerHTML = assignText;
		return -1;
	} else {
		var allAssignments = getAllAssignments.split("&");
		var assignText = "<form id =\"topic_list\" class=\"topic_list\">";
		assignText += "<input type=\"hidden\" name=\"class\" value=\"" + courseIDs + "\">";
		assignText += "<table><tr>";
		var assignID = [];
		for(var i = 0; i < allAssignments.length; i += 2) {
			assignText += "<td>";
			assignText += "<button type=\"botton\" value =\"" + getCourseId(allAssignments[i]) + "\" id=\"h_botton" + i / 2 + "\" name=\"homework\">" + getCourseId(allAssignments[i + 1]) + "</button>";
			assignText += "</td>";
			assignID[i / 2] = getCourseId(allAssignments[i]);
		}
		var accountType = getAccountType();
		if(accountType == "student") {
			assignText += "<div id=\"grads_check\"><button type=\"submit\" value=\"Grades\" id=\"check_button\" name =\"grades\" onclick=\"location.href='Show_grades.html'\">Check Grades</button></div>";
		} else if(accountType == "instructor") {
			assignText += "<div id=\"grads_check\"><button type=\"submit\" value=\"Grades\" id=\"check_button\" name =\"grades\" onclick=\"location.href='add_grade.html'\">Add Grades</button></div>";
		}
		assignText += "</tr></table></form>";
		document.getElementById("lists_of_topics").innerHTML = assignText;
		return assignID;
	}
}

function getAllMyDiscussions(courseIDs, assignIDs) {
	var getAllDiscussions = getDiscussions(assignIDs);
	if(getAllDiscussions == "") {
		var discussionText = "<form id=\"disc_list\" class=\"disc_list\">";
		discussionText += "<input type=\"hidden\" name=\"class\" value=\"" + courseIDs + "\">";
		discussionText += "<input type=\"hidden\" name=\"homework\" value=\"" + assignIDs + "\">";
		discussionText += "<table><tr><td>No discussions for this course.</td></tr></table>";
		discussionText += "</form>";
		document.getElementById("menu").innerHTML = discussionText;
		return -1;
	} else {
		var allDiscussions = getAllDiscussions.split("&");
		var discussionText = "<form id=\"disc_list\" class=\"disc_list\">";
		discussionText += "<input type=\"hidden\" name=\"class\" value=\"" + courseIDs + "\">";
		discussionText += "<input type=\"hidden\" name=\"homework\" value=\"" + assignIDs + "\">";
		discussionText += "<table id=\"discussion_list\">";
		var issueID = [];
		for(var i = 0; i < allDiscussions.length; i += 2) {
			discussionText += "<tr id=\"discussion_row\">";
			discussionText += "<td>";
			discussionText += "<label id=\"discussion_" + i / 2 + "content\">" + getCourseId(allDiscussions[i + 1]) + "</label>"
			discussionText += "</td><td>";
			discussionText += "<button type=\"botton\" value=\"" + getCourseId(allDiscussions[i]) + "\" id=\"jumpd_botton\" name=\"discid\">Jump</button>";
			discussionText += "</td></tr>";
			issueID[i / 2] = getCourseId(allDiscussions[i]);
		}
		discussionText += "<tr id=\"discussion_row\"><td>";
		discussionText += "<button type=\"button\" id=\"jumpd_botton\" onclick=\"location.href='new_discussion.html?course=" + courseIDs + "&homework=" + assignIDs + "'\">New Discussion</button>";
		discussionText += "</td><td></td></tr>"
		discussionText += "</table></form>";
		document.getElementById("menu").innerHTML = discussionText;
		return issueID;
	}
}

function getMyDiscussionDetails(courseIDs, assignIDs, issueIDs) {
	var getAllDetails = getDiscussionDetails(issueIDs);
	var allDetails = getAllDetails.split("&");
	var detailText = "<div id=\"discussion_title\">";
	detailText += getCourseId(allDetails[1]);
	detailText += "</div><div id=\"discussion_content\"><label id=\"discussion1_content\">";
	detailText += getCourseId(allDetails[2]);
	detailText += "</label></div>";
	for(var i = 3; i < allDetails.length; i++) {
		if(allDetails[i].indexOf("studentResponse") > -1) {
			if(!(getCourseId(allDetails[i]) == "")) {
				detailText += "<div id=\"discussion_content\"><label id=\"discussion1_content\">";
				detailText += "<b>Student Response:</b><br>";
				detailText += getCourseId(allDetails[i]);
				detailText += "</label></div>";
			}
		} else if(allDetails[i].indexOf("instructorResponse") > -1) {
			if(!(getCourseId(allDetails[i]) == "")) {
				detailText += "<div id=\"discussion_content\"><label id=\"discussion1_content\">";
				detailText += "<b>Instructor Response:</b><br>";
				detailText += getCourseId(allDetails[i]);
				detailText += "</label></div>";
			}
		}
	}
	detailText += "<div id=\"discussion_content\"><button type=\"submit\" name=\"newResponseBtn\" onclick=\"location.href='new_discussion.html?course=" + courseIDs + "&homework=" + assignIDs + "&discid=" + issueIDs + "'\">New Response</button></div>";
	document.getElementById("content").innerHTML = detailText;
}

function getQueryString() {
	var query_string = {};
	var query = window.location.search.substring(1);
  	var vars = query.split("&");
  	for (var i=0;i<vars.length;i++) {
    	var pair = vars[i].split("=");
    	if (typeof query_string[pair[0]] === "undefined") {
      		query_string[pair[0]] = pair[1];
    	} else if (typeof query_string[pair[0]] === "string") {
      		var arr = [ query_string[pair[0]], pair[1] ];
      		query_string[pair[0]] = arr;
    	} else {
      		query_string[pair[0]].push(pair[1]);
    	}
  	} 
  	return query_string;
}

function showGrades() {
	var stuName = getStudentAccount();
	var actualName = stuName.split("&");
	var gradesText = "<div id=\"grade_name\">Grades: <label name=\"student_name\">" + getCourseId(actualName[0]) + " " + getCourseId(actualName[1]) + "</label>";
	var getAllGrades = getGrades();
	var allGrades = getAllGrades.split("&");
	var courseNum = -1;
	var assignmentNum = 0;
	for(var i = 0; i < allGrades.length; i++) {
		if(allGrades[i].indexOf("courseName") > -1) {
			courseNum++;
			assignmentNum = 0;
			gradesText += "<div id=\"grades_c\">";
			gradesText += "<label name=\"class_name_" + courseNum + "\">" + getCourseId(allGrades[i]) + "</label>&nbsp; &nbsp; &nbsp; &nbsp; ";
		} else if(allGrades[i].indexOf("averageGrade") > -1) {
			gradesText += "Average: <label name=\"class" + courseNum + "average\">";
			gradesText += getCourseId(allGrades[i]);
			gradesText += "</label>";
		} else if(allGrades[i].indexOf("assignmentName") > -1) {
			assignmentNum++;
			if(assignmentNum == 1) {
				gradesText += "<div id=\"grades_c_all\"><table id=\"grades_class\">";
			}
			gradesText += "<tr><td><label name=\"topic_name_" + assignmentNum + "\">";
			gradesText += getCourseId(allGrades[i]);
			gradesText += "</label></td><td><label name=\"grades_t1\">";
			gradesText += getCourseId(allGrades[i+1]);
			gradesText += "</label></td></tr>";
			if(i + 2 < allGrades.length) {
				if(allGrades[i+2].indexOf("assignmentName") < 0) {
					gradesText += "</table></div></div>"
				}
			}
			i++;
		}
	}
	gradesText += "</div>";
	document.getElementById("grades_panel").innerHTML = gradesText;
}
