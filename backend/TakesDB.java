import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.util.ArrayList;

public class TakesDB extends Database
{
	protected Connection conn;

	public TakesDB(String uri) throws Exception
	{
		super(uri);

		final String query  =	"CREATE TABLE IF NOT EXISTS takes"+
								"(email TEXT,"+
								"courseID INTEGER,"+
								"FOREIGN KEY(email) "+
								"REFERENCES student(email),"+
								"FOREIGN KEY(courseID) "+
								"REFERENCES course(courseID));";

		Statement stmnt;

		this.conn = Database.getConnection(uri);
		stmnt = this.conn.createStatement();

		stmnt.execute(query);
	}

	public void createTakes(String email, int courseID)
	throws Exception
	{
		final String query = "INSERT INTO takes VALUES(?,?);";
		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setInt(2, courseID);

		stmnt.execute();
	}

	public void enrollCourse(String email, int year, String semester, String courseName)
	throws Exception
	{

		final String courseQuery = 	"SELECT courseID "+
									"FROM course c "+
									"WHERE c.year = ? AND c.semester = ? AND c.courseName = ?;";
		PreparedStatement stmntCourseQuery = this.conn.prepareStatement(courseQuery);
		stmntCourseQuery.setInt(1, year);
		stmntCourseQuery.setString(2, semester);
		stmntCourseQuery.setString(3, courseName);
		ResultSet rsCourseQuery = stmntCourseQuery.executeQuery();
		rsCourseQuery.next();

		final String query = "INSERT INTO takes VALUES(?,?);";
		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setInt(2, rsCourseQuery.getInt(1));

		stmnt.execute();
	}

	// updateTakes is not possible due to the many-to-many relationship
	// use delete and create instead

	public boolean deleteTakes(String email, int courseID)
	throws Exception
	{
		final String query =	"DELETE FROM takes "+
								"WHERE email=? AND courseID=?;";

		boolean result = false;
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setInt(2, courseID);

		result = (stmnt.executeUpdate() > 0);

		return result;
	}

	public ArrayList<String[]> getCourses(String email)
	throws Exception
	{
		final String query =	"SELECT c.courseID, courseName "+
								"FROM course c, takes t "+
								"WHERE t.email=? AND c.courseID = t.courseID;";

		PreparedStatement stmnt;
		ResultSet rs;
		ArrayList<String[]> results = new ArrayList<String[]>();
		String[] resultToAdd;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );
		stmnt.setString(1, email);

		rs = stmnt.executeQuery();

		while (rs.next()) {
			resultToAdd = new String[2];
			resultToAdd[0] = "" + rs.getString(1);
			resultToAdd[1] = rs.getString(2);
			results.add(resultToAdd);
		}
		return results;
	}
}
