import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.util.ArrayList;

public class LecturesDB extends Database
{
	protected Connection conn;

	public LecturesDB(String uri) throws Exception
	{
		super(uri);

		final String query  =	"CREATE TABLE IF NOT EXISTS lectures"+
								"(email TEXT,"+
								"courseID INTEGER,"+
								"FOREIGN KEY(email) "+
								"REFERENCES instructor(email),"+
								"FOREIGN KEY(courseID) "+
								"REFERENCES course(courseID));";

		Statement stmnt;

		this.conn = Database.getConnection(uri);
		stmnt = this.conn.createStatement();

		stmnt.execute(query);
	}

	public void createLectures(String email, int courseID)
	throws Exception
	{
		final String query = "INSERT INTO lectures VALUES(?,?);";
		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setInt(2, courseID);

		stmnt.execute();
	}

	// updateLectures is not possible due to the many-to-many relationship
	// use delete and create instead

	public boolean deleteLectures(String email, int courseID)
	throws Exception
	{
		final String query =	"DELETE FROM lectures "+
								"WHERE email=? AND courseID=?;";

		boolean result = false;
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setInt(2, courseID);

		result = (stmnt.executeUpdate() > 0);

		return result;
	}

	public ArrayList<String[]> getCourses(String email)
	throws Exception
	{
		final String query =	"SELECT c.courseID, courseName "+
								"FROM course c, lectures l "+
								"WHERE l.email=? AND c.courseID = l.courseID;";

		PreparedStatement stmnt;
		ResultSet rs;
		ArrayList<String[]> results = new ArrayList<String[]>();
		String[] resultToAdd;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );
		stmnt.setString(1, email);

		rs = stmnt.executeQuery();

		while (rs.next()) {
			resultToAdd = new String[2];
			resultToAdd[0] = "" + rs.getString(1);
			resultToAdd[1] = rs.getString(2);
			results.add(resultToAdd);
		}
		return results;
	}

}
