import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.util.ArrayList;

public class WorksOnDB extends Database
{
	protected Connection conn;

	public WorksOnDB(String uri) throws Exception
	{
		super(uri);

		final String query  =	"CREATE TABLE IF NOT EXISTS workson"+ // TODO why workson instead of worksOn?
								"(email TEXT,"+
								"assignmentID INTEGER,"+
								"grade REAL,"+
								"FOREIGN KEY(email) "+
								"REFERENCES student(email),"+
								"FOREIGN KEY(assignmentID) "+
								"REFERENCES assignment(assignmentID));";

		Statement stmnt;

		this.conn = Database.getConnection(uri);
		stmnt = this.conn.createStatement();

		stmnt.execute(query);
	}

	public void createWorksOn(String email, int assignmentID, double grade)
	throws Exception
	{
		final String query = "INSERT INTO workson VALUES(?,?,?);";
		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setInt(2, assignmentID);
		stmnt.setDouble(3, grade);

		stmnt.execute();
	}

	public void assignGrade(String email, String year, String semester, String courseName, String description, double grade)
	throws Exception
	{
		final String assignmentQuery = 	"SELECT assignmentID "+
										"FROM assignment a, course c "+
										"WHERE a.courseID = c.courseID AND c.year = ? AND c.semester = ? AND c.courseName = ? AND description = ?;";
		PreparedStatement stmntAssignmentQuery = this.conn.prepareStatement(assignmentQuery);
		stmntAssignmentQuery.setString(1, year);
		stmntAssignmentQuery.setString(2, semester);
		stmntAssignmentQuery.setString(3, courseName);
		stmntAssignmentQuery.setString(4, description);
		ResultSet rsAssignmentQuery = stmntAssignmentQuery.executeQuery();
		rsAssignmentQuery.next();

		final String query = 	"INSERT INTO workson "+
								"VALUES(?,?,?);";
		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setString(2, rsAssignmentQuery.getString(1));
		stmnt.setDouble(3, grade);

		stmnt.execute();
	}

	public boolean updateWorksOn(String email, int assignmentID, double grade)
	throws Exception
	{
		final String query =	"UPDATE workson "+
								"SET grade=?"+
								"WHERE email=? AND assignmentID=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setDouble(1, grade);
		stmnt.setString(2, email);
		stmnt.setInt(3, assignmentID);

		return (stmnt.executeUpdate() > 0);
	}

	public String[] getWorksOn(String email, int assignmentID)
	throws Exception
	{
		final String query =	"SELECT grade "+
								"FROM workson "+
								"WHERE email=? AND assignmentID=?;";

		PreparedStatement stmnt;
		ResultSet rs;
		String[] result;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmnt.setString(1, email);
		stmnt.setInt(2, assignmentID);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		rs.next();

		result = new String[1];

		for(int i = 0; i < 1; i++)
		{
			result[i] = rs.getString(i+1);
		}

		return result;
	}

	public boolean deleteWorksOn(String email, int assignmentID)
	throws Exception
	{
		final String query =	"DELETE FROM workson "+
								"WHERE email=? AND assignmentID=?;";

		boolean result = false;
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, email);
		stmnt.setInt(2, assignmentID);

		result = (stmnt.executeUpdate() > 0);

		return result;
	}

	public ArrayList<String[]> getGrades(String email)
	throws Exception
	{
		final String queryAllGrades =	"SELECT courseName, description, grade "+
										"FROM course c, assignment a, workson w "+
										"WHERE w.email=? AND w.assignmentID = a.assignmentID AND a.courseID = c.courseID;";

		final String queryAvgGrades =	"SELECT AVG(grade) "+
										"FROM course c, assignment a, workson w "+
										"WHERE w.email=? AND w.assignmentID = a.assignmentID AND a.courseID = c.courseID "+
										"GROUP BY c.courseID;";

		PreparedStatement stmntAll;
		PreparedStatement stmntAvg;
		ResultSet rsAll;
		ResultSet rsAvg;
		ArrayList<String[]> results = new ArrayList<String[]>();
		String[] resultToAdd;

		stmntAll = this.conn.prepareStatement(	queryAllGrades,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );
		stmntAvg = this.conn.prepareStatement(	queryAvgGrades,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmntAll.setString(1, email);
		stmntAvg.setString(1, email);

		rsAll = stmntAll.executeQuery();
		rsAvg = stmntAvg.executeQuery();
	
		String previousCourseName = "";
		while (rsAll.next()) {
			resultToAdd = new String[4];
			if (!rsAll.getString(1).equals(previousCourseName)) {
				previousCourseName = rsAll.getString(1);
				rsAvg.next();
			}
			resultToAdd[0] = rsAll.getString(1);
			resultToAdd[1] = rsAvg.getString(1);
			resultToAdd[2] = rsAll.getString(2);
			resultToAdd[3] = "" + rsAll.getDouble(3);
			results.add(resultToAdd);
		}
		return results;
	}
}
