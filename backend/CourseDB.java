import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;

public class CourseDB extends Database
{
	protected Connection conn;

	public CourseDB(String uri) throws Exception
	{
		super(uri);

		final String query  =	"CREATE TABLE IF NOT EXISTS course"+
								"(courseID SERIAL NOT NULL,"+
								"courseName TEXT,"+	// TODO we should be consistent. either name or courseName
								"year INTEGER,"+
								"semester TEXT,"+
								"PRIMARY KEY (courseID));";

		Statement stmnt;

		this.conn = Database.getConnection(uri);
		stmnt = this.conn.createStatement();

		stmnt.execute(query);
	}

	public int createCourse(String name, int year, String semester)
	throws Exception
	{
		final String query = "INSERT INTO course VALUES(DEFAULT,?,?,?);";
		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(	query,
											Statement.RETURN_GENERATED_KEYS );

		stmnt.setString(1, name);
		stmnt.setInt(2, year);
		stmnt.setString(3, semester);

		stmnt.execute();

		rs = stmnt.getGeneratedKeys();

		// There should only be one key generated
		rs.next();

		return rs.getInt(1);
	}

	public boolean updateCourse(int courseID, String name, int year, String semester)
	throws Exception
	{
		final String query =	"UPDATE course "+
								"SET courseName=?,year=?,semester=?"+
								"WHERE courseID=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1, name);
		stmnt.setInt(2, year);
		stmnt.setString(3, semester);
		stmnt.setInt(4, courseID);

		return (stmnt.executeUpdate() > 0);
	}

	public String[] getCourse(int courseID)
	throws Exception
	{
		final String query =	"SELECT courseName,year,semester "+
								"FROM course "+
								"WHERE courseID=?;";

		PreparedStatement stmnt;
		ResultSet rs;
		String[] result;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmnt.setInt(1, courseID);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		rs.next();

		result = new String[3];

		for(int i = 0; i < 3; i++)
		{
			result[i] = rs.getString(i+1);
		}

		return result;
	}

	public boolean deleteCourse(int courseID)
	throws Exception
	{
		final String query = "DELETE FROM course WHERE courseID=?;";
		boolean result = false;
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setInt(1, courseID);

		result = (stmnt.executeUpdate() > 0);

		return result;
	}

}
