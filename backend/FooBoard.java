public class FooBoard
{
	private static String readFromStdin()
	throws Exception
	{
		String contentLengthStr = System.getenv("CONTENT_LENGTH");
		int contentLength = 0;
		byte[] buf = null;
		int offset = 0;

		if(!contentLengthStr.isEmpty())
		{
			contentLength = Integer.parseInt(contentLengthStr);
		}

		buf = new byte[contentLength];

		while(offset < contentLength)
		{
			int bytesRead = System.in.read(buf, offset, contentLength-offset);

			if(bytesRead == -1)
			{
				System.err.println("Error: I/O error.");

				offset = contentLength;
			}
			else
			{
				offset += bytesRead;
			}
		}

		return new String(buf);
	}

	public static void main(String[] args)
	{
		String response = null;
		String cookie = null;

		try
		{
			Rpc rpc = new Rpc(readFromStdin());
			String[] rpcResult = rpc.exec();

			response = rpcResult[0];
			cookie = rpcResult[1];

			if(response == null)
			{
				response = "";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		if(cookie != null)
		{
			System.out.printf(	"Content-Type: text/plain\r\n"+
								// TODO: Re-enable secure flag
								//"St-Cookie: %s; Secure; HttpOnly\r\n"+
								"Set-Cookie: hash=%s; HttpOnly\r\n"+
								"Content-Length: %d\r\n\r\n%s",
								cookie,
								response.length(),
								response);
		}
		else
		{
			System.out.printf(	"Content-Type: text/plain\r\n"+
								"Content-Length: %d\r\n\r\n%s",
								response.length(),
								response);
		}
	}
}
