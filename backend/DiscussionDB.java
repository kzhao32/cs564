import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.util.ArrayList;

public class DiscussionDB extends Database
{
	protected Connection conn;

	public DiscussionDB(String uri) throws Exception
	{
		super(uri);

		final String query  =	"CREATE TABLE IF NOT EXISTS discussion"+
								"(discussionID SERIAL NOT NULL,"+
								"assignmentID INTEGER,"+
								"issueNumber INTEGER NOT NULL,"+
								"time TEXT,"+
								"summary TEXT,"+
								"details TEXT,"+
								"discussionType TEXT,"+
								"studentResponse TEXT,"+
								"instructorResponse TEXT,"+
								"PRIMARY KEY (discussionID),"+
								"FOREIGN KEY(assignmentID) "+
								"REFERENCES assignment(assignmentID));";

		Statement stmnt;

		this.conn = Database.getConnection(uri);
		stmnt = this.conn.createStatement();

		stmnt.execute(query);
	}

	public int createDiscussion(	int assignmentID,
									int issueNumber,
									String time,
									String summary,
									String details,
									String discussionType,
									String studentResponse,
									String instructorResponse )
	throws Exception
	{
		final String query =	"INSERT INTO discussion "+
								"VALUES(DEFAULT,?,?,?,?,?,?,?,?);";

		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(	query,
											Statement.RETURN_GENERATED_KEYS );

		stmnt.setInt(1, assignmentID);
		stmnt.setInt(2, issueNumber);
		stmnt.setString(3,time);
		stmnt.setString(4,summary);
		stmnt.setString(5,details);
		stmnt.setString(6,discussionType);
		stmnt.setString(7,studentResponse);
		stmnt.setString(8,instructorResponse);

		stmnt.execute();

		rs = stmnt.getGeneratedKeys();

		// There should only be one key generated
		rs.next();

		return rs.getInt(1);
	}

	public String addNewDiscussion(	int assignmentID,
									String time,
									String type,
									String summary,
									String details )
	throws Exception
	{
		PreparedStatement stmnt;
		ResultSet rs;
		final String issueQuery =	"SELECT MAX(issueNumber) "+
									"FROM discussion;";
		stmnt = this.conn.prepareStatement(	issueQuery,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );
		rs = stmnt.executeQuery();
		rs.next();
		int issueNumber = rs.getInt(1) + 1;

		final String query =	"INSERT INTO discussion "+
								"VALUES(DEFAULT,?,?,?,?,?,?,?,?);";
		stmnt = this.conn.prepareStatement(	query,
											Statement.RETURN_GENERATED_KEYS );
		stmnt.setInt(1, assignmentID);
		stmnt.setInt(2, issueNumber);
		stmnt.setString(3,time);
		stmnt.setString(4,summary);
		stmnt.setString(5,details);
		stmnt.setString(6,type);
		stmnt.setString(7,"");
		stmnt.setString(8,"");
		stmnt.execute();
		rs = stmnt.getGeneratedKeys();
		rs.next();

		rs.getInt(1);

		return "" + issueNumber;
	}

	public int addFollowupDiscussion(	int issueNumber,
										String time,
										String type,
										String summary,
										String details )
	throws Exception
	{
		final String assignmentQuery = 	"SELECT assignmentID "+
										"FROM discussion d "+
										"WHERE issueNumber = ?;";
		PreparedStatement stmntAssignmentQuery = this.conn.prepareStatement(assignmentQuery);
		stmntAssignmentQuery.setInt(1, issueNumber);
		ResultSet rsAssignmentQuery = stmntAssignmentQuery.executeQuery();
		rsAssignmentQuery.next();

		final String query =	"INSERT INTO discussion "+
								"VALUES(DEFAULT,?,?,?,?,?,?,?,?);";
		PreparedStatement stmnt;
		ResultSet rs;
		stmnt = this.conn.prepareStatement(	query,
											Statement.RETURN_GENERATED_KEYS );

		stmnt.setInt(1, rsAssignmentQuery.getInt(1));
		stmnt.setInt(2, issueNumber);
		stmnt.setString(3,time);
		stmnt.setString(4,summary);
		stmnt.setString(5,details);
		stmnt.setString(6,type);
		stmnt.setString(7,"");
		stmnt.setString(8,"");

		stmnt.execute();

		rs = stmnt.getGeneratedKeys();

		// There should only be one key generated
		rs.next();

		return rs.getInt(1);
	}

	public boolean updateDiscussion(int discussionID,
									int assignmentID,
									int issueNumber,
									String time,
									String summary,
									String details,
									String discussionType,
									String studentResponse,
									String instructorResponse )
	throws Exception
	{
		final String query =	"UPDATE discussion "+
								"SET assignmentID=?,issueNumber=?,time=?,summary=?,details=?,"+
								"discussionType=?,studentResponse=?,instructorResponse=?"+
								"WHERE discussionID=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setInt(1, assignmentID);
		stmnt.setInt(2, issueNumber);
		stmnt.setString(3,time);
		stmnt.setString(4,summary);
		stmnt.setString(5,details);
		stmnt.setString(6,discussionType);
		stmnt.setString(7,studentResponse);
		stmnt.setString(8,instructorResponse);
		stmnt.setInt(9,discussionID);

		return (stmnt.executeUpdate() > 0);
	}

	public boolean addInstructorResponse(	int discussionID,
											String instructorResponse )
	throws Exception
	{
		final String query =	"UPDATE discussion "+
								"SET instructorResponse=?"+
								"WHERE discussionID=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1,instructorResponse);
		stmnt.setInt(2,discussionID);

		return (stmnt.executeUpdate() > 0);
	}

	public boolean addStudentResponse(	int discussionID,
										String studentResponse )
	throws Exception
	{
		final String query =	"UPDATE discussion "+
								"SET studentResponse=?"+
								"WHERE discussionID=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setString(1,studentResponse);
		stmnt.setInt(2,discussionID);

		return (stmnt.executeUpdate() > 0);
	}

	public String[] getDiscussion(int discussionID)
	throws Exception
	{
		final String query =	"SELECT assignmentID,issueNumber,time,summary,"+
								"details,discussionType,studentResponse,"+
								"instructorResponse "+
								"FROM discussion "+
								"WHERE discussionID=?;";

		PreparedStatement stmnt;
		ResultSet rs;
		String[] result;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmnt.setInt(1, discussionID);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		rs.next();

		result = new String[8];

		for(int i = 0; i < 8; i++)
		{
			result[i] = rs.getString(i+1);
		}

		return result;
	}

	public ArrayList<String[]> getDiscussions(int assignmentID)
	throws Exception
	{
		final String query =	"SELECT d.issueNumber, d.summary "+
								"FROM discussion d "+
								"WHERE d.assignmentID=? AND d.summary != '';";

		PreparedStatement stmnt;
		ResultSet rs;
		ArrayList<String[]> results = new ArrayList<String[]>();
		String[] resultToAdd;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );
		stmnt.setInt(1, assignmentID);

		rs = stmnt.executeQuery();

		while (rs.next()) {
			resultToAdd = new String[2];
			resultToAdd[0] = "" + rs.getString(1);
			resultToAdd[1] = rs.getString(2);
			results.add(resultToAdd);
		}
		return results;
	}

	public ArrayList<String[]> getDiscussionDetails(int issueNumber)
	throws Exception
	{
		final String query =	"SELECT discussionType, summary, details, instructorResponse, studentResponse "+
								"FROM discussion "+
								"WHERE issueNumber=?;";
		PreparedStatement stmnt;
		ResultSet rs;
		ArrayList<String[]> results = new ArrayList<String[]>();
		String[] resultToAdd;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );
		stmnt.setInt(1, issueNumber);

		rs = stmnt.executeQuery();

		while (rs.next()) {
			resultToAdd = new String[5];
			resultToAdd[0] = rs.getString(1);
			resultToAdd[1] = rs.getString(2);
			resultToAdd[2] = rs.getString(3);
			resultToAdd[3] = rs.getString(4);
			resultToAdd[4] = rs.getString(5);
			results.add(resultToAdd);
		}
		return results;
	}

	public boolean deleteDiscussion(int discussionID)
	throws Exception
	{
		final String query = "DELETE FROM discussion WHERE discussionID=?;";
		boolean result = false;
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setInt(1, discussionID);

		result = (stmnt.executeUpdate() > 0);

		return result;
	}
}
