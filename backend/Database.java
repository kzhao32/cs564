import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public abstract class Database
{
	private static Connection sharedConn = null;

	public Database(String uri) throws Exception
	{
		// TODO: Switch to MySQL
		Class.forName("org.postgresql.Driver");
	}

	public static Connection getConnection(String uri) throws Exception
	{
		if(Database.sharedConn == null)
		{
			Properties props = new Properties();

			props.setProperty("user", "user1");
			props.setProperty("password", "password");

			Database.sharedConn = DriverManager.getConnection(uri, props);
		}

		return Database.sharedConn;
	}
}
