#!/bin/bash

SRV_URI="http://localhost:8000/cgi-bin/FooBoard.cgi"
TMPDIR="/tmp/"

function server_poll
{
	count=0

	curl -s $SRV_URI

	while (( ($? != 0) && (count < 50) ))
	do
		sleep 0.1
		echo "Waiting for server to become ready..."
		curl -s $SRV_URI
	done

	if [ $? != 0 ]
	then
		echo "Failed to start server" >&2
		exit 1
	fi
}

function test_req
{
	ret=$(curl	-s -c ${TMPDIR}/cookie -b ${TMPDIR}/cookie \
			--request POST $SRV_URI --data "$1")

	if [ "$ret" != "$2" ]
	then
		echo "Test failed: Expecting \"$2\". Got \"$ret\"." >&2
		exit 1
	fi
}

function cleanup
{
	kill -TERM $pid
	rm -f ${TMPDIR}/cookie
}

cd $(dirname $0)/../frontend
>cgi-bin/backend.db
python3 -m http.server --cgi &
#python3 -m http.server --cgi &>/dev/null &
trap cleanup EXIT

pid=$!

server_poll

# Student accounts

echo "Testing student account creation..."

test_req	"method=createStudentAccount&email=jdoe@example.com&password=pass&firstName=John&lastName=Doe&isGraduate=0"\
		"SUCCESS"

echo "Testing duplicate student account creation..."

test_req	"method=createStudentAccount&email=jdoe@example.com&password=pass&firstName=John&lastName=Doe&isGraduate=0"\
		""

echo "Testing student account retrieval..."
test_req	"method=login&email=jdoe@example.com&password=pass"\
		"SUCCESS"
test_req	"method=getStudentAccount"\
		"firstName=John&lastName=Doe&isGraduate=0"

echo "Testing person account update..."
test_req	"method=createStudentAccount&email=student@example.com&password=pass&firstName=Stu&lastName=Dent&isGraduate=1"\
		"SUCCESS"
test_req	"method=updatePersonAccount&password=pass&firstName=Qwe&lastName=Asd"\
		"SUCCESS"

echo "Testing student account update..."
test_req	"method=login&email=jdoe@example.com&password=pass"\
		"SUCCESS"
test_req	"method=updateStudentAccount&isGraduate=0"\
		"SUCCESS"

echo "Testing updated student account retrieval..."
test_req	"method=login&email=student@example.com&password=pass"\
		"SUCCESS"
test_req	"method=getStudentAccount"\
		"firstName=Stu&lastName=Dent&isGraduate=1"

echo "Testing student account update to update just person..."
test_req	"method=updateStudentAccount&password=pass&firstName=Stu&lastName=Doctor&isGraduate=0"\
		"SUCCESS"
test_req	"method=getStudentAccount"\
		"firstName=Stu&lastName=Doctor&isGraduate=0"

echo "Testing student account update to update just student..."
test_req	"method=updateStudentAccount&password=pass&firstName=Stud&lastName=Doctor&isGraduate=1"\
		"SUCCESS"
test_req	"method=getStudentAccount"\
		"firstName=Stud&lastName=Doctor&isGraduate=1"

echo "Testing student account update to update person and student..."
test_req	"method=updateStudentAccount&password=pass&firstName=NewFirstName&lastName=Doctor&isGraduate=0"\
		"SUCCESS"
test_req	"method=getStudentAccount"\
		"firstName=NewFirstName&lastName=Doctor&isGraduate=0"

echo "Testing student account deletion..."
test_req	"method=login&email=jdoe@example.com&password=pass"\
		"SUCCESS"
test_req	"method=deleteAccount"\
		"SUCCESS"

echo "Testing student account deletion with empty table..."
test_req	"method=deleteAccount"\
		""

# Instructor accounts
echo "Testing instructor account creation..."

test_req	"method=createInstructorAccount&email=jdoe@example.com&password=pass&firstName=John&lastName=Doe&officeHourTime=foo&officeHourLocation=bar"\
		"SUCCESS"

echo "Testing duplicate instructor account creation..."

test_req	"method=createInstructorAccount&email=jdoe@example.com&password=pass&firstName=John&lastName=Doe&officeHourTime=foo&officeHourLocation=bar"\
		""

echo "Testing instructor account retrieval..."

test_req	"method=login&email=jdoe@example.com&password=pass"\
		"SUCCESS"
test_req	"method=getInstructorAccount"\
		"firstName=John&lastName=Doe&officeHourTime=foo&officeHourLocation=bar"

echo "Testing instructor account update..."
test_req	"method=createInstructorAccount&email=instructor@example.com&password=pass&firstName=Ins&lastName=Tructor&officeHourTime=420&officeHourLocation=colorado"\
		"SUCCESS"
test_req	"method=login&email=instructor@example.com&password=pass"\
		"SUCCESS"
test_req	"method=updateInstructorAccount&officeHourTime=419.999&officeHourLocation=denver"\
		"SUCCESS"

echo "Testing updated instructor account retrieval..."
test_req	"method=getInstructorAccount"\
		"firstName=Ins&lastName=Tructor&officeHourTime=419.999&officeHourLocation=denver"

echo "Testing instructor account update to update just person..."
test_req	"method=updateInstructorAccount&password=pass&firstName=Abstructor&lastName=Teacher&officeHourTime=419.999&officeHourLocation=denver"\
		"SUCCESS"
test_req	"method=login&email=instructor@example.com&password=pass"\
		"SUCCESS"
test_req	"method=getInstructorAccount"\
		"firstName=Abstructor&lastName=Teacher&officeHourTime=419.999&officeHourLocation=denver"

echo "Testing instructor account update to update just instructor..."
test_req	"method=updateInstructorAccount&password=pass&firstName=Abstructor&lastName=Teacher&officeHourTime=911&officeHourLocation=denver"\
		"SUCCESS"
test_req	"method=getInstructorAccount"\
		"firstName=Abstructor&lastName=Teacher&officeHourTime=911&officeHourLocation=denver"

echo "Testing instructor account update to update person and instructor..."
test_req	"method=updateInstructorAccount&password=pass&firstName=lastName&lastName=firstName&officeHourTime=311&officeHourLocation=south"\
		"SUCCESS"
test_req	"method=getInstructorAccount"\
		"firstName=lastName&lastName=firstName&officeHourTime=311&officeHourLocation=south"

echo "Testing instructor account deletion..."

test_req	"method=login&email=jdoe@example.com&password=pass"\
		"SUCCESS"
test_req	"method=deleteAccount"\
		"SUCCESS"

echo "Testing instructor account deletion with empty table..."

test_req	"method=deleteAccount"\
		""

# Courses

echo "Testing course creation..."

test_req	"method=createCourse&name=foo&year=1234&semester=bar"\
		"1"

echo "Testing duplicate course creation..."

test_req	"method=createCourse&name=foo&year=1234&semester=bar"\
		"2"

echo "Testing course update..."
test_req	"method=createCourse&name=gym&year=2015&semester=notSummer"\
		"3"
test_req	"method=updateCourse&courseID=3&name=wine&year=2021&semester=spring"\
		"SUCCESS"

echo "Testing course update with nonexistent course..."
test_req	"method=updateCourse&courseID=24&name=beer&year=2042&semester=summer"\
		""

echo "Testing updated course retrieval..."
test_req	"method=getCourse&courseID=3"\
		"courseName=wine&year=2021&semester=spring"

echo "Testing course retrieval..."

test_req	"method=getCourse&courseID=1"\
		"courseName=foo&year=1234&semester=bar"

echo "Testing course deletion..."

test_req	"method=deleteCourse&courseID=2"\
		"SUCCESS"

# Assignments

echo "Testing assignment creation..."

test_req	"method=createAssignment&courseID=1&type=foo&assignDate=bar&dueDate=foobar&description=barfoo"\
		"1"

echo "Testing duplicate assignment creation..."

test_req	"method=createAssignment&courseID=1&type=foo&assignDate=bar&dueDate=foobar&description=barfoo"\
		"2"

echo "Testing assignment creation with nonexistent courseID..."

test_req	"method=createAssignment&courseID=2&type=foo&assignDate=bar&dueDate=foobar&description=barfoo"\
		""

echo "Testing assignment update..."
test_req	"method=createAssignment&courseID=1&type=atype&assignDate=aday&dueDate=dday&description=ddesc"\
		"3"
test_req	"method=updateAssignment&assignmentID=3&courseID=1&type=btype&assignDate=bday&dueDate=bbday&description=bdesc"\
		"SUCCESS"

echo "Testing assignment update with nonexistent assignmentID..."
test_req	"method=updateAssignment&assignmentID=90&courseID=1&type=Bnegative&assignDate=soon&dueDate=tomorrow&description=theCowJumpedOverTheMoon"\
		""

echo "Testing updated assignment retrieval..."
test_req	"method=getAssignment&assignmentID=3"\
		"courseID=1&type=btype&assignDate=bday&dueDate=bbday&description=bdesc"

echo "Testing assignment retrieval..."

test_req	"method=getAssignment&assignmentID=1"\
		"courseID=1&type=foo&assignDate=bar&dueDate=foobar&description=barfoo"

echo "Testing assignment deletion..."

test_req	"method=deleteAssignment&assignmentID=2"\
		"SUCCESS"

# Discussions

echo "Testing discussion creation..."

test_req	"method=createDiscussion&assignmentID=1&issueNumber=1&time=foo&summary=bar&details=foobar&discussionType=barfoo&studentResponse=baz&instructorResponse=zab"\
		"1"

echo "Testing duplicate discussion creation..."

test_req	"method=createDiscussion&assignmentID=1&issueNumber=1&time=foo&summary=bar&details=foobar&discussionType=barfoo&studentResponse=baz&instructorResponse=zab"\
		"2"

echo "Testing discussion creation with nonexistent assignmentID..."

test_req	"method=createDiscussion&assignmentID=0&issueNumber=1&time=foo&summary=bar&details=foobar&discussionType=barfoo&studentResponse=baz&instructorResponse=zab"\
		""

echo "Testing discussion update..."
test_req	"method=createDiscussion&assignmentID=1&issueNumber=1&time=dynamite&summary=sum&details=deed&discussionType=dType&studentResponse=stuRes&instructorResponse=no"\
		"3"
test_req	"method=updateDiscussion&discussionID=3&assignmentID=3&issueNumber=1&time=stop&summary=jen&details=jes&discussionType=con&studentResponse=stuying&instructorResponse=insann"\
		"SUCCESS"

echo "Testing discussion update with nonexistent discussionID..."
test_req	"method=updateDiscussion&discussionID=34&assignmentID=1&issueNumber=1&time=t1&summary=s1&details=d1&discussionType=dt1&studentResponse=sr1&instructorResponse=ir1"\
		""

echo "Testing updated discussion retrieval..."
test_req	"method=getDiscussion&discussionID=3"\
		"assignmentID=3&issueNumber=1&time=stop&summary=jen&details=jes&discussionType=con&studentResponse=stuying&instructorResponse=insann"

echo "Testing add student response..."
test_req	"method=createDiscussion&assignmentID=3&issueNumber=2&time=t4&summary=s4&details=d4&discussionType=d4&studentResponse=ns4&instructorResponse=ni4"\
		"4"
test_req	"method=addStudentResponse&discussionID=4&studentResponse=s4"\
		"SUCCESS"

echo "Testing add student response to nonexistent discussionID..."
test_req	"method=addStudentResponse&discussionID=41&studentResponse=s4ne"\
		""

echo "Testing updated student response retrieval..."
test_req	"method=getDiscussion&discussionID=4"\
		"assignmentID=3&issueNumber=2&time=t4&summary=s4&details=d4&discussionType=d4&studentResponse=s4&instructorResponse=ni4"

echo "Testing add instructor response..."
test_req	"method=addInstructorResponse&discussionID=4&instructorResponse=i4"\
		"SUCCESS"

echo "Testing add instructor response to nonexistent discussionID..."
test_req	"method=addInstructorResponse&discussionID=41&instructorResponse=i4ne"\
		""

echo "Testing updated instructor response retrieval..."
test_req	"method=getDiscussion&discussionID=4"\
		"assignmentID=3&issueNumber=2&time=t4&summary=s4&details=d4&discussionType=d4&studentResponse=s4&instructorResponse=i4"

echo "Testing discussion retrieval..."

test_req	"method=getDiscussion&discussionID=1"\
		"assignmentID=1&issueNumber=1&time=foo&summary=bar&details=foobar&discussionType=barfoo&studentResponse=baz&instructorResponse=zab"

echo "Testing discussion deletion..."

test_req	"method=deleteDiscussion&discussionID=2"\
		"SUCCESS"

# Lectures

echo "Creating instructor account..."

test_req	"method=createInstructorAccount&email=jdoe@example.com&password=pass&firstName=John&lastName=Doe&officeHourTime=foo&officeHourLocation=bar"\
		"SUCCESS"
test_req	"method=login&email=jdoe@example.com&password=pass"\
		"SUCCESS"

echo "Testing lectures creation..."

test_req	"method=createLectures&courseID=1"\
		"SUCCESS"

echo "Testing lectures creation with nonexistent courseID..."

test_req	"method=createLectures&email=jdoe@example.com&courseID=0"\
		""

echo "Testing lectures deletion..."

test_req	"method=deleteLectures&courseID=1"\
		"SUCCESS"

# Takes

echo "Creating student account..."

test_req	"method=createStudentAccount&email=eodj@example.com&password=pass&firstName=John&lastName=Doe&isGraduate=0"\
		"SUCCESS"

echo "Testing takes creation..."

test_req	"method=createTakes&email=eodj@example.com&courseID=1"\
		"SUCCESS"

echo "Testing takes creation with nonexistent student email..."

test_req	"method=createTakes&email=eodje@example.com&courseID=1"\
		""

echo "Testing takes creation with nonexistent courseID..."

test_req	"method=createTakes&email=eodj@example.com&courseID=0"\
		""

echo "Testing takes deletion..."

test_req	"method=deleteTakes&email=eodj@example.com&courseID=1"\
		"SUCCESS"

# WorksOn

echo "Testing worksOn creation..."

test_req	"method=createWorksOn&email=eodj@example.com&assignmentID=1&grade=42"\
		"SUCCESS"

echo "Testing worksOn creation with nonexistent student email..."

test_req	"method=createWorksOn&email=eodje@example.com&assignmentID=1&grade=42"\
		""

echo "Testing worksOn creation with nonexistent assignmentID..."

test_req	"method=createWorksOn&email=eodj@example.com&assignmentID=0&grade=42"\
		""

echo "Testing worksOn update..."
test_req	"method=createWorksOn&email=student@example.com&assignmentID=3&grade=68"\
		"SUCCESS"
test_req	"method=updateWorksOn&email=student@example.com&assignmentID=3&grade=70.1"\
		"SUCCESS"

echo "Testing updated worksOn retrieval..."
test_req	"method=getWorksOn&email=student@example.com&assignmentID=3"\
		"grade=70.1"

echo "Testing worksOn deletion..."

test_req	"method=deleteWorksOn&email=eodj@example.com&assignmentID=1"\
		"SUCCESS"

# Website APIs

echo "Setting up data to test website APIs..."
echo "	Creating instructors to test website APIs..."
test_req	"method=createInstructorAccount&email=inst1&password=pass&firstName=inst1&lastName=i1&officeHourTime=time1&officeHourLocation=location1"\
		"SUCCESS"
test_req	"method=createInstructorAccount&email=inst2&password=pass&firstName=inst2&lastName=i2&officeHourTime=time2&officeHourLocation=location2"\
		"SUCCESS"
test_req	"method=createInstructorAccount&email=inst3&password=pass&firstName=inst3&lastName=i3&officeHourTime=time3&officeHourLocation=location3"\
		"SUCCESS"
echo "	Creating students to test website APIs..."
test_req	"method=createStudentAccount&email=stud1&password=pass&firstName=stud1&lastName=s1&isGraduate=1"\
		"SUCCESS"
test_req	"method=createStudentAccount&email=stud2&password=pass&firstName=stud2&lastName=s2&isGraduate=0"\
		"SUCCESS"
test_req	"method=createStudentAccount&email=stud3&password=pass&firstName=stud3&lastName=s3&isGraduate=1"\
		"SUCCESS"
echo "	Creating courses to test website APIs..."
test_req	"method=createCourse&name=course1&year=1&semester=1"\
		"4"
test_req	"method=createCourse&name=course2&year=2&semester=2"\
		"5"
test_req	"method=createCourse&name=course3&year=3&semester=3"\
		"6"
echo "	Creating lectures to test website APIs..."
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=createLectures&courseID=4"\
		"SUCCESS"
test_req	"method=login&email=inst2&password=pass"\
		"SUCCESS"
test_req	"method=createLectures&courseID=4"\
		"SUCCESS"
test_req	"method=login&email=inst3&password=pass"\
		"SUCCESS"
test_req	"method=createLectures&courseID=4"\
		"SUCCESS"
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=createLectures&courseID=5"\
		"SUCCESS"
test_req	"method=login&email=inst2&password=pass"\
		"SUCCESS"
test_req	"method=createLectures&courseID=5"\
		"SUCCESS"
echo "	Creating takes to test website APIs..."
test_req	"method=createTakes&email=stud1&courseID=4"\
		"SUCCESS"
test_req	"method=createTakes&email=stud2&courseID=4"\
		"SUCCESS"
test_req	"method=createTakes&email=stud3&courseID=4"\
		"SUCCESS"
test_req	"method=createTakes&email=stud1&courseID=5"\
		"SUCCESS"
test_req	"method=createTakes&email=stud2&courseID=5"\
		"SUCCESS"
echo "	Creating assignments to test website APIs..."
test_req	"method=createAssignment&courseID=4&type=type1&assignDate=assignDate1&dueDate=dueDate1&description=description1"\
		"4"
test_req	"method=createAssignment&courseID=4&type=type2&assignDate=assignDate2&dueDate=dueDate2&description=description2"\
		"5"
test_req	"method=createAssignment&courseID=4&type=type3&assignDate=assignDate3&dueDate=dueDate3&description=description3"\
		"6"
test_req	"method=createAssignment&courseID=5&type=type4&assignDate=assignDate4&dueDate=dueDate4&description=description4"\
		"7"
test_req	"method=createAssignment&courseID=5&type=type5&assignDate=assignDate5&dueDate=dueDate5&description=description5"\
		"8"
echo "	Creating worksOn to test website APIs..."
test_req	"method=createWorksOn&email=stud1&assignmentID=4&grade=70"\
		"SUCCESS"
test_req	"method=createWorksOn&email=stud2&assignmentID=4&grade=75"\
		"SUCCESS"
test_req	"method=createWorksOn&email=stud3&assignmentID=4&grade=80"\
		"SUCCESS"
test_req	"method=createWorksOn&email=stud1&assignmentID=5&grade=85"\
		"SUCCESS"
test_req	"method=createWorksOn&email=stud2&assignmentID=5&grade=90"\
		"SUCCESS"
test_req	"method=createWorksOn&email=stud1&assignmentID=6&grade=95"\
		"SUCCESS"
test_req	"method=createWorksOn&email=stud1&assignmentID=7&grade=100"\
		"SUCCESS"

	# get grades

echo "Getting grades..."
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=getGrades"\
		"courseName=course1&averageGrade=83.3333333333333&assignmentName=description1&grade=70.0&assignmentName=description2&grade=85.0&assignmentName=description3&grade=95.0&courseName=course2&averageGrade=100.0&assignmentName=description4&grade=100.0"

	# assign grades

echo "Assigning grade..."
test_req	"method=assignGrade&email=stud1&year=2&semester=2&courseName=course2&description=description5&grade=65"\
		"SUCCESS"
test_req	"method=getWorksOn&email=stud1&assignmentID=8"\
		"grade=65.0"

echo "Assigning grade with bad student email..."
test_req	"method=assignGrade&email=bad&year=2&semester=2&courseName=course2&description=description5&grade=65"\
		""

echo "Assigning grade with bad year..."
test_req	"method=assignGrade&email=stud1&year=bad&semester=2&courseName=course2&description=description5&grade=65"\
		""

echo "Assigning grade with bad semester..."
test_req	"method=assignGrade&email=stud1&year=2&semester=bad&courseName=course2&description=description5&grade=65"\
		""

echo "Assigning grade with bad courseName..."
test_req	"method=assignGrade&email=stud1&year=2&semester=2&courseName=bad&description=description5&grade=65"\
		""

echo "Assigning grade with bad description..."
test_req	"method=assignGrade&email=stud1&year=2&semester=2&courseName=course2&description=bad&grade=65"\
		""

echo "testing addCourse without logging in..."
test_req	"method=logout"\
		"SUCCESS"
test_req	"method=addCourse&year=1&semester=semester1&courseName=courseName1"\
		""

	# add course

echo "testing addCourse by logging in as a student..."
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=addCourse&year=1&semester=semester1&courseName=courseName1"\
		""

echo "testing addCourse by logging in as an instrcutor, should pass..."
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=addCourse&year=1&semester=semester1&courseName=courseName1"\
		"SUCCESS"

echo "testing enrollCourse without logging in..."
test_req	"method=logout"\
		"SUCCESS"
test_req	"method=enrollCourse&year=3&semester=3&courseName=course3"\
		""

	# enroll course

echo "testing enrollCourse by not loggin in..."
test_req	"method=logout"\
		"SUCCESS"
test_req	"method=enrollCourse&year=3&semester=3&courseName=course3"\
		""

echo "testing enrollCourse by logging in as an instrcutor..."
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=enrollCourse&year=3&semester=3&courseName=course3"\
		""

echo "testing that student is not currently enrolled in course"
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=getCourses"\
		"courseID=4&courseName=course1&courseID=5&courseName=course2"

echo "testing enrollCourse by logging in as a student, should pass..."
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=enrollCourse&year=3&semester=3&courseName=course3"\
		"SUCCESS"

echo "testing that student is now enrolled in course"
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=getCourses"\
		"courseID=4&courseName=course1&courseID=5&courseName=course2&courseID=6&courseName=course3"

echo "testing enrollCourse with bad year not int..."
test_req	"method=enrollCourse&year=bad&semester=3&courseName=course3"\
		""

echo "testing enrollCourse with bad year for no course..."
test_req	"method=enrollCourse&year=2&semester=bad&courseName=course3"\
		""

echo "testing enrollCourse with bad semester..."
test_req	"method=enrollCourse&year=1&semester=bad&courseName=course3"\
		""

echo "testing enrollCourse with bad courseName..."
test_req	"method=enrollCourse&year=1&semester=bad&courseName=bad"\
		""

	# add assignment

echo "testing addAssignment by not loggin in..."
test_req	"method=logout"\
		"SUCCESS"
test_req	"method=addAssignment&courseID=5&type=type6&assignDate=assignDate6&dueDate=dueDate6&description=description6"\
		""

echo "testing addAssignment by logging in as an instrcutor, should pass..."
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=addAssignment&courseID=5&type=type6&assignDate=assignDate6&dueDate=dueDate6&description=description6"\
		"SUCCESS"

echo "testing addAssignment with bad courseID not int..."
test_req	"method=addAssignment&courseID=bad&type=type6&assignDate=assignDate6&dueDate=dueDate6&description=description6"\
		""

echo "testing addAssignment with bad year for no course..."
test_req	"method=addAssignment&courseID=9000&type=type6&assignDate=assignDate6&dueDate=dueDate6&description=description6"\
		""

echo "testing addAssignment by logging in as a student..."
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=addAssignment&courseID=5&type=type6&assignDate=assignDate6&dueDate=dueDate6&description=description6"\
		""

	# add New Discussion

echo "testing add New Discussion..."
test_req	"method=addNewDiscussion&assignmentID=4&time=time1&type=type1&summary=summary1&details=details1"\
		"issueNumber=3"
test_req	"method=addNewDiscussion&assignmentID=4&time=time2&type=type2&summary=summary2&details=details2"\
		"issueNumber=4"
test_req	"method=addNewDiscussion&assignmentID=4&time=time3&type=type3&summary=summary3&details=details3"\
		"issueNumber=5"

echo "testing add New Discussion with bad assignmentID, not int..."
test_req	"method=addNewDiscussion&assignmentID=bad&time=time1&type=type1&summary=summary1&details=details1"\
		""

echo "testing add New Discussion with bad assignmentID, does not exist..."
test_req	"method=addNewDiscussion&assignmentID=9000&time=time1&type=type1&summary=summary1&details=details1"\
		""

	# add Followup Discussion

echo "testing add followup discussion..."
test_req	"method=addFollowupDiscussion&issueNumber=1&time=time4&details=details4"\
		"SUCCESS"

echo "testing add followup discussion again on same issue..."
test_req	"method=addFollowupDiscussion&issueNumber=1&time=time5&details=details5"\
		"SUCCESS"

echo "testing add followup discussion again on new issue..."
test_req	"method=addFollowupDiscussion&issueNumber=2&time=time6&details=details6"\
		"SUCCESS"

echo "testing add followup discussion with bad issueNumber not int..."
test_req	"method=addFollowupDiscussion&issueNumber=bad&time=time7&details=details7"\
		""

echo "testing add followup discussion with bad issueNumber, does not exist..."
test_req	"method=addFollowupDiscussion&issueNumber=90210&time=time8&details=details8"\
		""

	# add Response

echo "testing add response without loggin in..."
test_req	"method=logout"\
		"SUCCESS"
test_req	"method=addResponse&discussionID=8&text=text1"\
		""

echo "testing add response by instructor..."
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=addResponse&discussionID=8&text=text2"\
		"SUCCESS"

echo "testing add response by student..."
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=addResponse&discussionID=8&text=text3"\
		"SUCCESS"

	# get courses

echo "testing getCourses without logging in..."
test_req	"method=logout"\
		"SUCCESS"
test_req	"method=getCourses"\
		""

echo "testing getCourses with student1..."
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=getCourses"\
		"courseID=4&courseName=course1&courseID=5&courseName=course2&courseID=6&courseName=course3"

echo "testing getCourses with student3..."
test_req	"method=login&email=stud3&password=pass"\
		"SUCCESS"
test_req	"method=getCourses"\
		"courseID=4&courseName=course1"

echo "testing getCourses with inst1..."
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=getCourses"\
		"courseID=4&courseName=course1&courseID=5&courseName=course2&courseID=7&courseName=courseName1"

echo "testing getCourses with inst3..."
test_req	"method=login&email=inst3&password=pass"\
		"SUCCESS"
test_req	"method=getCourses"\
		"courseID=4&courseName=course1"

	# get assignments

echo "testing get assignments1..."
test_req	"method=getAssignments&courseID=1"\
		"assignmentID=1&assignmentName=barfoo&assignmentID=3&assignmentName=bdesc"

echo "testing get assignments2..."
test_req	"method=getAssignments&courseID=4"\
		"assignmentID=4&assignmentName=description1&assignmentID=5&assignmentName=description2&assignmentID=6&assignmentName=description3"

echo "testing get assignments3..."
test_req	"method=getAssignments&courseID=5"\
		"assignmentID=7&assignmentName=description4&assignmentID=8&assignmentName=description5&assignmentID=9&assignmentName=description6"

echo "testing get assignments with bad courseID, not int..."
test_req	"method=getAssignments&courseID=bad"\
		""

echo "testing get assignments with bad courseID, courseID does not exist..."
test_req	"method=getAssignments&courseID=90210"\
		""

	# get discussions

echo "testing get discussion1..."
test_req	"method=getDiscussions&assignmentID=1"\
		"issueNumber=1&summary=bar"

echo "testing get discussions2..."
test_req	"method=getDiscussions&assignmentID=3"\
		"issueNumber=1&summary=jen&issueNumber=2&summary=s4"

echo "testing get discussions3..."
test_req	"method=getDiscussions&assignmentID=4"\
		"issueNumber=3&summary=summary1&issueNumber=4&summary=summary2&issueNumber=5&summary=summary3"

echo "testing get discussions4, no such discussion..."
test_req	"method=getDiscussions&assignmentID=5"\
		""

echo "testing get discussions, assignmentID not int..."
test_req	"method=getDiscussions&assignmentID=bad"\
		""

echo "testing get discussions2, assignmentID does not exist..."
test_req	"method=getDiscussions&assignmentID=90210"\
		""

	# get discussion details
echo "testing get discussion details1..."
test_req	"method=getDiscussionDetails&issueNumber=1"\
		"type=barfoo&summary=bar&details=foobar&instructorResponse=zab&studentResponse=baz&type=con&summary=jen&details=jes&instructorResponse=insann&studentResponse=stuying&type=followup&summary=&details=details4&instructorResponse=text2&studentResponse=text3&type=followup&summary=&details=details5&instructorResponse=&studentResponse="

echo "testing get discussion details2..."
test_req	"method=getDiscussionDetails&issueNumber=2"\
		"type=d4&summary=s4&details=d4&instructorResponse=i4&studentResponse=s4&type=followup&summary=&details=details6&instructorResponse=&studentResponse="

echo "testing get discussion details3..."
test_req	"method=getDiscussionDetails&issueNumber=3"\
		"type=type1&summary=summary1&details=details1&instructorResponse=&studentResponse="

echo "testing get discussion details with bad issueNumber not an int..."
test_req	"method=getDiscussionDetails&issueNumber=bad"\
		""

echo "testing get discussion details with abd issueNumber that does not exist..."
test_req	"method=getDiscussionDetails&issueNumber=90210"\
		""

	# get Account Type
echo "testing get account type without logging in..."
test_req	"method=logout"\
		"SUCCESS"
test_req	"method=getAccountType"\
		""

echo "testing get account type logging in as instructor..."
test_req	"method=login&email=inst1&password=pass"\
		"SUCCESS"
test_req	"method=getAccountType"\
		"instructor"

echo "testing get get account type logging in as student..."
test_req	"method=login&email=stud1&password=pass"\
		"SUCCESS"
test_req	"method=getAccountType"\
		"student"


echo "Done, all test cases passed!"
