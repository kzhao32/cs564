import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;

public class AccountDB extends Database
{
	protected Connection conn;

	public enum AccountType
	{
		Student,
		Instructor,
		Unknown
	}

	public AccountDB(String uri) throws Exception
	{
		super(uri);

		final String personQuery =	"CREATE TABLE IF NOT EXISTS person"+
									"(email TEXT NOT NULL,"+
									"password TEXT,"+
									"firstName TEXT,"+
									"lastName TEXT,"+
									"PRIMARY KEY (email));";

		final String instructorQuery =	"CREATE TABLE IF NOT EXISTS instructor"+
										"(email TEXT NOT NULL,"+
										"officeHourTime TEXT,"+
										"officeHourLocation TEXT,"+
										"PRIMARY KEY (email),"+
										"FOREIGN KEY(email) "+
										"REFERENCES person(email));";

		final String studentQuery =	"CREATE TABLE IF NOT EXISTS student"+
									"(email TEXT NOT NULL,"+
									"isGraduate BOOLEAN,"+
									"PRIMARY KEY (email),"+
									"FOREIGN KEY(email) "+
									"REFERENCES person(email));";

		Statement stmnt;

		this.conn = Database.getConnection(uri);
		stmnt = this.conn.createStatement();

		stmnt.execute(personQuery);
		stmnt.execute(instructorQuery);
		stmnt.execute(studentQuery);
	}

	public String getEmailFromHash(String hash)
	throws Exception
	{
		final String query = "SELECT email from person WHERE password=?;";
		PreparedStatement stmnt = this.conn.prepareStatement(query);
		ResultSet rs;

		stmnt.setString(1, hash);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		rs.next();

		return rs.getString(1);
	}

	public void addInstructor(	String email,
								String password,
								String firstName,
								String lastName,
								String officeHourTime,
								String officeHourLocation )
	throws Exception
	{
		// TODO: validate email
		final String personQuery = "INSERT INTO person VALUES(?,?,?,?);";
		final String instructorQuery = "INSERT INTO instructor VALUES(?,?,?);";
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(personQuery);

		stmnt.setString(1, email);
		stmnt.setString(2, Authenticator.getSaltedHash(email+password));
		stmnt.setString(3, firstName);
		stmnt.setString(4, lastName);

		stmnt.execute();

		stmnt = this.conn.prepareStatement(instructorQuery);

		stmnt.setString(1, email);
		stmnt.setString(2, officeHourTime);
		stmnt.setString(3, officeHourLocation);

		stmnt.execute();
	}

	public void addStudent(	String email,
							String password,
							String firstName,
							String lastName,
							boolean isGraduate )
	throws Exception
	{
		// TODO: validate email
		final String personQuery = "INSERT INTO person VALUES(?,?,?,?);";
		final String studentQuery = "INSERT INTO student VALUES(?,?);";
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(personQuery);

		stmnt.setString(1, email);
		stmnt.setString(2, Authenticator.getSaltedHash(email+password));
		stmnt.setString(3, firstName);
		stmnt.setString(4, lastName);

		stmnt.execute();

		stmnt = this.conn.prepareStatement(studentQuery);

		stmnt.setString(1, email);
		stmnt.setBoolean(2, isGraduate);

		stmnt.execute();
	}

	public boolean updatePerson(String email,
								String password,
								String firstName,
								String lastName )
	throws Exception
	{
		final String update =	"UPDATE person "+
								"SET password=?,firstName=?,lastName=?"+
								"WHERE email=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(update);

		stmnt.setString(1, Authenticator.getSaltedHash(email+password));
		stmnt.setString(2, firstName);
		stmnt.setString(3, lastName);
		stmnt.setString(4, email);

		return (stmnt.executeUpdate() > 0);
	}

	public boolean updateInstructor(String email,
									String officeHourTime,
									String officeHourLocation )
	throws Exception
	{
		final String update =	"UPDATE instructor "+
								"SET officeHourTime=?,officeHourLocation=?"+
								"WHERE email=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(update);

		stmnt.setString(1, officeHourTime);
		stmnt.setString(2, officeHourLocation);
		stmnt.setString(3, email);

		return (stmnt.executeUpdate() > 0);
	}

	public boolean updateInstructor(String email,
									String password,
									String firstName,
									String lastName,
									String officeHourTime,
									String officeHourLocation )
	throws Exception
	{
		final String personUpdate =	"UPDATE person "+
									"SET password=?,firstName=?,lastName=?"+
									"WHERE email=?;";
		final String instructorUpdate =	"UPDATE instructor "+
										"SET officeHourTime=?,officeHourLocation=?"+
										"WHERE email=?;";

		PreparedStatement personStmnt;

		personStmnt = this.conn.prepareStatement(personUpdate);

		personStmnt.setString(1, Authenticator.getSaltedHash(email+password));
		personStmnt.setString(2, firstName);
		personStmnt.setString(3, lastName);
		personStmnt.setString(4, email);

		PreparedStatement instructorStmnt;

		instructorStmnt = this.conn.prepareStatement(instructorUpdate);

		instructorStmnt.setString(1, officeHourTime);
		instructorStmnt.setString(2, officeHourLocation);
		instructorStmnt.setString(3, email);

		return (personStmnt.executeUpdate() + instructorStmnt.executeUpdate() > 0);
	}

	public boolean updateStudent(	String email,
									boolean isGraduate )
	throws Exception
	{
		final String query =	"UPDATE student "+
								"SET isGraduate=?"+
								"WHERE email=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setBoolean(1, isGraduate);
		stmnt.setString(2, email);

		return (stmnt.executeUpdate() > 0);
	}

	public boolean updateStudent(	String email,
								String password,
								String firstName,
								String lastName,
								boolean isGraduate )
	throws Exception
	{
		final String personUpdate =	"UPDATE person "+
									"SET password=?,firstName=?,lastName=?"+
									"WHERE email=?;";
		final String studentUpdate =	"UPDATE student "+
										"SET isGraduate=?"+
										"WHERE email=?;";

		PreparedStatement personStmnt;

		personStmnt = this.conn.prepareStatement(personUpdate);

		personStmnt.setString(1, Authenticator.getSaltedHash(email+password));
		personStmnt.setString(2, firstName);
		personStmnt.setString(3, lastName);
		personStmnt.setString(4, email);

		PreparedStatement studentStmnt;

		studentStmnt = this.conn.prepareStatement(studentUpdate);

		studentStmnt.setBoolean(1, isGraduate);
		studentStmnt.setString(2, email);

		return (personStmnt.executeUpdate() + studentStmnt.executeUpdate() > 0);
	}

	public String getInstructor(String email)
	throws Exception
	{
		final String query =	"SELECT firstName,Lastname,"+
								"officeHourTime,officeHourLocation "+
								"FROM Person p,Instructor i "+
								"WHERE p.email=i.email AND p.email=?;";

		PreparedStatement stmnt;
		ResultSet rs;
		String[] result;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmnt.setString(1, email);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		result = new String[4];
		if (rs.next()) {

			result = new String[4];

			for(int i = 0; i < 4; i++)
			{
				result[i] = rs.getString(i+1);
			}
		}

		if (result[0] != null || result[1] != null || result[2] != null || result[3] != null) {
				return String.format
						(	"firstName=%s&lastName=%s&officeHourTime=%s"+
							"&officeHourLocation=%s",
							result[0],
							result[1],
							result[2],
							result[3] );
		}

		return "";
	}

	public String getStudent(String email)
	throws Exception
	{
		final String query =	"SELECT firstName,Lastname,isGraduate "+
								"FROM Person p,Student s "+
								"WHERE p.email=s.email AND p.email=?;";

		PreparedStatement stmnt;
		ResultSet rs;
		String[] result;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmnt.setString(1, email);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		result = new String[3];
		if (rs.next()) {

			result = new String[3];

			for(int i = 0; i < 3; i++)
			{
				result[i] = rs.getString(i+1);
			}
		}

		if (result[0] != null || result[1] != null || result[2] != null) {
				return String.format
						(	"firstName=%s&lastName=%s&isGraduate=%s",
							result[0],
							result[1],
							result[2] );
		}

		return "";
	}

	public boolean deleteAccount(String email)
	throws Exception
	{
		// TODO: validate email
		final String personQuery =	"DELETE FROM person WHERE email=?;";
		final String studentQuery =	"DELETE FROM student WHERE email=?;";
		final String instructorQuery =	"DELETE FROM instructor WHERE email=?;";

		PreparedStatement stmnt;
		boolean result = true;

		stmnt = this.conn.prepareStatement(instructorQuery);

		stmnt.setString(1, email);

		stmnt.execute();

		stmnt = this.conn.prepareStatement(studentQuery);

		stmnt.setString(1, email);

		stmnt.execute();

		stmnt = this.conn.prepareStatement(personQuery);

		stmnt.setString(1, email);

		return (stmnt.executeUpdate() > 0);
	}

	public String getPasswordHash(String email)
	throws Exception
	{
		final String query =	"SELECT password from person "+
								"WHERE email = ?;";

		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmnt.setString(1, email);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		rs.next();

		return rs.getString(1);
	}

	public boolean authenticate(	String email,
									String password )
	throws Exception
	{
		return Authenticator.verify(	email+password,
										this.getPasswordHash(email) );
	}
}
