import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * Handles verification of user credentials
 */
public class Authenticator
{
	/**
	 * A string denoting the hashing algorithm to use.
	 */
	public static final String hashAlgo = "SHA-512";

	/**
	 * The length of the salt in bytes.
	 */
	public static final int saltLen = 8;

	/**
	 * Generate salted hash for given string.
	 *
	 * @param	data	The string for which to generate salted hash
	 * @return	String representation of the salted hash corresponding to the
	 *			given data string.
	 */
	public static String getSaltedHash(String data)
	throws Exception
	{
		String salt = Authenticator.generateSalt();
		Base64.Encoder encoder = Base64.getEncoder();

		return Authenticator.getHash(data+salt)+salt;
	}

	/**
	 * Check whether or not the given string matches the one used to generate
	 * the salted hash.
	 *
	 * @param	data		The string to verify the salted hash against
	 * @param	saltedHash	The salted hash to use to verify the given string
	 * @return	True if the given data string matches the salted hash. False
	 *			otherwise.
	 */
	public static boolean verify(String data, String saltedHash)
	throws Exception
	{
		MessageDigest md = MessageDigest.getInstance(Authenticator.hashAlgo);
		final int hashLen = (int)Math.ceil((md.getDigestLength()/3.0)*4);
		String salt = saltedHash.substring(hashLen);
		String newHash = Authenticator.getHash(data+salt);

		return saltedHash.equals(newHash+salt);
	}

	/**
	 * Generate hash from the given string.
	 *
	 * @param	data	The string to use to generate the hash
	 * @return	String representation of the hash of the given data string.
	 */
	private static String getHash(String data)
	throws Exception
	{
		MessageDigest md = MessageDigest.getInstance(Authenticator.hashAlgo);
		Base64.Encoder encoder = Base64.getEncoder().withoutPadding();

		md.update(data.getBytes());

		return encoder.encodeToString(md.digest());
	}

	/**
	 * Generate a random string of characters to be used as salt.
	 *
	 * @return	A random string of characters.
	 */
	private static String generateSalt()
	{
		SecureRandom random = new SecureRandom();
		Base64.Encoder encoder = Base64.getEncoder().withoutPadding();
		byte[] randomBytes = new byte[Authenticator.saltLen];

		random.nextBytes(randomBytes);

		return encoder.encodeToString(randomBytes);
	}
}
