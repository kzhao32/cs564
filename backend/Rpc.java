import java.util.StringTokenizer;
import java.util.ArrayList;

public class Rpc extends RpcParser
{
	public final String SUCCESS_STR = "SUCCESS";
	public final String db_uri = "jdbc:postgresql://boardfoo.chcwfyg4pdzo.us-west-2.rds.amazonaws.com/fooboard";
	private AccountDB accountDB;
	private CourseDB courseDB;
	private AssignmentDB assignmentDB;
	private DiscussionDB discussionDB;
	private LecturesDB lecturesDB;
	private TakesDB takesDB;
	private WorksOnDB worksOnDB;

	public Rpc()
	throws Exception
	{
		init();
	}

	public Rpc(String str)
	throws Exception
	{
		super(str);

		init();
	}

	public String[] exec()
	{
		String response = null;
		String cookie = null;

		try
		{
			if(this.method.equals("login"))
			{
				response = login();
				cookie = this.accountDB.getPasswordHash(this.getParam("email"));
			}
			else if(this.method.equals("logout"))
			{
				// This can never fail
				response = SUCCESS_STR;
				cookie = "";
			}
			else if(this.method.equals("createInstructorAccount"))
			{
				response = createInstructorAccount();
			}
			else if(this.method.equals("createStudentAccount"))
			{
				response = createStudentAccount();
			}
			else if(this.method.equals("getInstructorAccount"))
			{
				response = getInstructorAccount();
			}
			else if(this.method.equals("getStudentAccount"))
			{
				response = getStudentAccount();
			}
			else if(this.method.equals("updatePersonAccount"))
			{
				String email
					= accountDB.getEmailFromHash(this.getHashFromCookie());

				response = updatePersonAccount();
				cookie = accountDB.getPasswordHash(email);
			}
			else if(this.method.equals("updateInstructorAccount"))
			{
				String email
					= accountDB.getEmailFromHash(this.getHashFromCookie());

				response = updateInstructorAccount();
				cookie = accountDB.getPasswordHash(email);
			}
			else if(this.method.equals("updateStudentAccount"))
			{
				String email
					= accountDB.getEmailFromHash(this.getHashFromCookie());

				response = updateStudentAccount();
				cookie = accountDB.getPasswordHash(email);
			}
			else if(this.method.equals("deleteAccount"))
			{
				response = deleteAccount();
			}
			else if(this.method.equals("createCourse"))
			{
				response = createCourse();
			}
			else if(this.method.equals("updateCourse"))
			{
				response = updateCourse();
			}
			else if(this.method.equals("getCourse"))
			{
				response = getCourse();
			}
			else if(this.method.equals("deleteCourse"))
			{
				response = deleteCourse();
			}
			else if(this.method.equals("createAssignment"))
			{
				response = createAssignment();
			}
			else if(this.method.equals("updateAssignment"))
			{
				response = updateAssignment();
			}
			else if(this.method.equals("getAssignment"))
			{
				response = getAssignment();
			}
			else if(this.method.equals("deleteAssignment"))
			{
				response = deleteAssignment();
			}
			else if(this.method.equals("createDiscussion"))
			{
				response = createDiscussion();
			}
			else if(this.method.equals("updateDiscussion"))
			{
				response = updateDiscussion();
			}
			else if(this.method.equals("addInstructorResponse"))
			{
				response = addInstructorResponse();
			}
			else if(this.method.equals("addStudentResponse"))
			{
				response = addStudentResponse();
			}
			else if(this.method.equals("getDiscussion"))
			{
				response = getDiscussion();
			}
			else if(this.method.equals("deleteDiscussion"))
			{
				response = deleteDiscussion();
			}
			else if(this.method.equals("createLectures"))
			{
				response = createLectures();
			}
			else if(this.method.equals("deleteLectures"))
			{
				response = deleteLectures();
			}
			else if(this.method.equals("createTakes"))
			{
				response = createTakes();
			}
			else if(this.method.equals("deleteTakes"))
			{
				response = deleteTakes();
			}
			else if(this.method.equals("createWorksOn"))
			{
				response = createWorksOn();
			}
			else if(this.method.equals("updateWorksOn"))
			{
				response = updateWorksOn();
			}
			else if(this.method.equals("getWorksOn"))
			{
				response = getWorksOn();
			}
			else if(this.method.equals("deleteWorksOn"))
			{
				response = deleteWorksOn();
			}
			else if(this.method.equals("getGrades"))
			{
				response = getGrades();
			}
			else if(this.method.equals("assignGrade"))
			{
				response = assignGrade();
			}
			else if(this.method.equals("addCourse"))
			{
				response = addCourse();
			}
			else if(this.method.equals("enrollCourse"))
			{
				response = enrollCourse();
			}
			else if(this.method.equals("addAssignment"))
			{
				response = addAssignment();
			}
			else if(this.method.equals("addNewDiscussion"))
			{
				response = addNewDiscussion();
			}
			else if(this.method.equals("addFollowupDiscussion"))
			{
				response = addFollowupDiscussion();
			}
			else if(this.method.equals("addResponse"))
			{
				response = addResponse();
			}
			else if(this.method.equals("getCourses"))
			{
				response = getCourses();
			}
			else if(this.method.equals("getAssignments"))
			{
				response = getAssignments();
			}
			else if(this.method.equals("getDiscussions"))
			{
				response = getDiscussions();
			}
			else if(this.method.equals("getDiscussionDetails"))
			{
				response = getDiscussionDetails();
			}
			else if(this.method.equals("getAccountType"))
			{
				response = getAccountType();
			}
			else
			{
				System.err.println("Error: RPC method not found.");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return new String[]{response, cookie};
	}

	private void init()
	throws Exception
	{
		this.accountDB = new AccountDB(db_uri);
		this.courseDB = new CourseDB(db_uri);
		this.assignmentDB = new AssignmentDB(db_uri);
		this.discussionDB = new DiscussionDB(db_uri);
		this.lecturesDB = new LecturesDB(db_uri);
		this.takesDB = new TakesDB(db_uri);
		this.worksOnDB = new WorksOnDB(db_uri);
	}

	private String getHashFromCookie()
	{
		String cookie = System.getenv("HTTP_COOKIE");
		StringTokenizer tokenizer = new StringTokenizer(cookie, ";");
		String result = "";
		boolean done = false;

		while(!done && tokenizer.hasMoreTokens())
		{
			String token = tokenizer.nextToken();
			int offset = token.indexOf('=');
			String key = token.substring(0, offset);
			String value = token.substring(offset+1);

			if(key.equals("hash"))
			{
				result = value;
				done = true;
			}
		}

		return result;
	}

	private String login()
	{
		String response = "";
		String email = null;
		String password = null;

		if(this.params.size() == 2)
		{
			email = this.getParam("email");
			password = this.getParam("password");
		}

		if(email != null && password != null)
		{
			boolean rc;

			try
			{
				rc = this.accountDB.authenticate(email, password);
			}
			catch(Exception e)
			{
				rc = false;
			}

			if(rc)
			{
				response = SUCCESS_STR;
			}
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createInstructorAccount()
	throws Exception
	{
		String response = "";
		String email = null;
		String password = null;
		String firstName = null;
		String lastName = null;
		String officeHourTime = null;
		String officeHourLocation = null;

		if(this.params.size() == 6)
		{
			email = this.getParam("email");
			password = this.getParam("password");
			firstName = this.getParam("firstName");
			lastName = this.getParam("lastName");
			officeHourTime = this.getParam("officeHourTime");
			officeHourLocation = this.getParam("officeHourLocation");
		}

		if(email != null
		&& password != null
		&& firstName != null
		&& lastName != null
		&& officeHourTime != null
		&& officeHourLocation != null)
		{
			this.accountDB.addInstructor(	email,
											password,
											firstName,
											lastName,
											officeHourTime,
											officeHourLocation );

			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createStudentAccount()
	throws Exception
	{
		String response = "";
		String email = null;
		String password = null;
		String firstName = null;
		String lastName = null;
		Boolean isGraduate = null;

		if(this.params.size() == 5)
		{
			email = this.getParam("email");
			password = this.getParam("password");
			firstName = this.getParam("firstName");
			lastName = this.getParam("lastName");
			if (this.getParam("isGraduate").equals("1")) {
				isGraduate = true;
			} else if (this.getParam("isGraduate").equals("0")) {
				isGraduate = false;
			} else {
				throw new Exception("Malformed boolean");
			}
		}

		if(email != null
		&& password != null
		&& firstName != null
		&& lastName != null
		&& isGraduate != null)
		{
			this.accountDB.addStudent(	email,
										password,
										firstName,
										lastName,
										isGraduate );

			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String updatePersonAccount()
	throws Exception
	{
		String response = "";
		String email = null;
		String password = null;
		String firstName = null;
		String lastName = null;
		boolean rc = false;

		if(this.params.size() == 3)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			password = this.getParam("password");
			firstName = this.getParam("firstName");
			lastName = this.getParam("lastName");
		}

		if(email != null
		&& password != null
		&& firstName != null
		&& lastName != null)
		{
			rc = this.accountDB.updatePerson(	email,
												password,
												firstName,
												lastName );
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String updateInstructorAccount()
	throws Exception
	{
		String response = "";
		String email = null;
		String password = null;
		String firstName = null;
		String lastName = null;
		String officeHourTime = null;
		String officeHourLocation = null;
		boolean rc = false;

		if(this.params.size() == 2)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			officeHourTime = this.getParam("officeHourTime");
			officeHourLocation = this.getParam("officeHourLocation");

			if(email != null
			&& officeHourTime != null
			&& officeHourLocation != null)
			{
				rc = this.accountDB.updateInstructor(	email,
														officeHourTime,
														officeHourLocation );
			}
		}
		else if(this.params.size() == 5)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			password = this.getParam("password");
			firstName = this.getParam("firstName");
			lastName = this.getParam("lastName");
			officeHourTime = this.getParam("officeHourTime");
			officeHourLocation = this.getParam("officeHourLocation");

			if(email != null
			&& password != null
			&& firstName != null
			&& lastName != null
			&& officeHourTime != null
			&& officeHourLocation != null)
			{
				rc = this.accountDB.updateInstructor(	email,
														password,
														firstName,
														lastName,
														officeHourTime,
														officeHourLocation );
			}
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String updateStudentAccount()
	throws Exception
	{
		String response = "";
		String email = null;
		String password = null;
		String firstName = null;
		String lastName = null;
		Boolean isGraduate = null;
		boolean rc = false;

		if(this.params.size() == 1)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			if (this.getParam("isGraduate").equals("1")) {
				isGraduate = true;
			} else if (this.getParam("isGraduate").equals("0")) {
				isGraduate = false;
			} else {
				throw new Exception("Malformed boolean");
			}

			if(email != null
			&& isGraduate != null)
			{
				rc = this.accountDB.updateStudent(	email,
													isGraduate );
			}
		}
		else if(this.params.size() == 4)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			password = this.getParam("password");
			firstName = this.getParam("firstName");
			lastName = this.getParam("lastName");
			if (this.getParam("isGraduate").equals("1")) {
				isGraduate = true;
			} else if (this.getParam("isGraduate").equals("0")) {
				isGraduate = false;
			} else {
				throw new Exception("Malformed boolean");
			}

			if(email != null
			&& password != null
			&& firstName != null
			&& lastName != null
			&& isGraduate != null)
			{
				rc = this.accountDB.updateStudent(	email,
													password,
													firstName,
													lastName,
													isGraduate );
			}
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getInstructorAccount()
	throws Exception
	{
		String response = "";
		String email = null;

		if(this.params.size() == 0)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
		}

		if(email != null)
		{
			return this.accountDB.getInstructor(email);
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getStudentAccount()
	throws Exception
	{
		String response = "";
		String email = null;

		if(this.params.size() == 0)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
		}

		if(email != null)
		{
			return this.accountDB.getStudent(email);
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String deleteAccount()
	throws Exception
	{
		String response = "";
		String email = null;

		if(this.params.size() == 0)
		{
			email = this.accountDB.getEmailFromHash(this.getHashFromCookie());
		}

		if(this.accountDB.deleteAccount(email))
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createCourse()
	throws Exception
	{
		String response = "";
		String name = null;
		int year = -1;
		String semester = null;

		if(this.params.size() == 3)
		{
			name = this.getParam("name");
			year = Integer.parseInt(this.getParam("year"));
			semester = this.getParam("semester");
		}

		if(name != null && year != -1 && semester != null)
		{
			int rc = this.courseDB.createCourse(name, year, semester);

			response = Integer.toString(rc);
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String updateCourse()
	throws Exception
	{
		String response = "";
		int courseID = -1;
		String name = null;
		int year = -1;
		String semester = null;
		boolean rc = false;

		if(this.params.size() == 4)
		{
			courseID = Integer.parseInt(this.getParam("courseID"));
			name = this.getParam("name");
			year = Integer.parseInt(this.getParam("year"));
			semester = this.getParam("semester");
		}

		if(name != null && year != -1 && semester != null)
		{
			rc = this.courseDB.updateCourse(courseID, name, year, semester);
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getCourse()
	throws Exception
	{
		String response = "";
		int courseID = -1;

		if(this.params.size() == 1)
		{
			courseID = Integer.parseInt(this.getParam("courseID"));
		}

		if(courseID != -1)
		{
			String[] columns = this.courseDB.getCourse(courseID);

			response = String.format
						(	"courseName=%s&year=%s&semester=%s",
							columns[0],
							columns[1],
							columns[2] );
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String deleteCourse()
	throws Exception
	{
		String response = "";
		int courseID = -1;

		if(this.params.size() == 1)
		{
			courseID = Integer.parseInt(this.getParam("courseID"));
		}

		if(courseID != -1 && this.courseDB.deleteCourse(courseID))
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createAssignment()
	throws Exception
	{
		String response = "";
		int courseID = -1;
		String type = null;
		String assignDate = null;
		String dueDate = null;
		String description = null;

		if(this.params.size() == 5)
		{
			courseID = Integer.parseInt(this.getParam("courseID"));
			type = this.getParam("type");
			assignDate = this.getParam("assignDate");
			dueDate = this.getParam("dueDate");
			description = this.getParam("description");

		}

		if(courseID != -1
		&& type != null
		&& assignDate != null
		&& dueDate != null
		&& description != null)
		{
			int rc = this.assignmentDB.createAssignment(	courseID,
															type,
															assignDate,
															dueDate,
															description );

			response = Integer.toString(rc);
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String updateAssignment()
	throws Exception
	{
		String response = "";
		int assignmentID = -1;
		int courseID = -1;
		String type = null;
		String assignDate = null;
		String dueDate = null;
		String description = null;
		boolean rc = false;

		if(this.params.size() == 6)
		{
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
			courseID = Integer.parseInt(this.getParam("courseID"));
			type = this.getParam("type");
			assignDate = this.getParam("assignDate");
			dueDate = this.getParam("dueDate");
			description = this.getParam("description");
		}

		if(assignmentID != -1
		&& courseID != -1
		&& type != null
		&& assignDate != null
		&& dueDate != null
		&& description != null)
		{
			rc = this.assignmentDB.updateAssignment(	assignmentID,
														courseID,
														type,
														assignDate,
														dueDate,
														description );
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getAssignment()
	throws Exception
	{
		String response = "";
		int assignmentID = -1;

		if(this.params.size() == 1)
		{
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
		}

		if(assignmentID != -1)
		{
			String[] columns = this.assignmentDB.getAssignment(assignmentID);

			response = String.format
						(	"courseID=%s&type=%s&assignDate=%s&dueDate=%s&"+
							"description=%s",
							columns[0],
							columns[1],
							columns[2],
							columns[3],
							columns[4] );
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String deleteAssignment()
	throws Exception
	{
		String response = "";
		int assignmentID = -1;

		if(this.params.size() == 1)
		{
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
		}

		if(assignmentID != -1
		&& this.assignmentDB.deleteAssignment(assignmentID))
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createDiscussion()
	throws Exception
	{
		String response = "";
		int assignmentID = -1;
		int issueNumber = -1;
		String time = null;
		String summary = null;
		String details = null;
		String discussionType = null;
		String studentResponse = null;
		String instructorResponse = null;

		if(this.params.size() == 8)
		{
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
			issueNumber = Integer.parseInt(this.getParam("issueNumber"));
			time = this.getParam("time");
			summary = this.getParam("summary");
			details = this.getParam("details");
			discussionType = this.getParam("discussionType");
			studentResponse = this.getParam("studentResponse");
			instructorResponse = this.getParam("instructorResponse");
		}

		if(assignmentID != -1
		&& issueNumber != -1
		&& time != null
		&& summary != null
		&& details != null
		&& discussionType != null
		&& studentResponse != null
		&& instructorResponse != null)
		{
			int rc = this.discussionDB.createDiscussion
						(	assignmentID,
							issueNumber,
							time,
							summary,
							details,
							discussionType,
							studentResponse,
							instructorResponse );

			response = Integer.toString(rc);
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String updateDiscussion()
	throws Exception
	{
		String response = "";
		int discussionID = -1;
		int assignmentID = -1;
		int issueNumber = -1;
		String time = null;
		String summary = null;
		String details = null;
		String discussionType = null;
		String studentResponse = null;
		String instructorResponse = null;
		boolean rc = false;

		if(this.params.size() == 9)
		{
			discussionID = Integer.parseInt(this.getParam("discussionID"));
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
			issueNumber = Integer.parseInt(this.getParam("issueNumber"));
			time = this.getParam("time");
			summary = this.getParam("summary");
			details = this.getParam("details");
			discussionType = this.getParam("discussionType");
			studentResponse = this.getParam("studentResponse");
			instructorResponse = this.getParam("instructorResponse");
		}

		if(discussionID != -1
		&& assignmentID != -1
		&& issueNumber != -1
		&& time != null
		&& summary != null
		&& details != null
		&& discussionType != null
		&& studentResponse != null
		&& instructorResponse != null)
		{
			rc = this.discussionDB.updateDiscussion(discussionID,
													assignmentID,
													issueNumber,
													time,
													summary,
													details,
													discussionType,
													studentResponse,
													instructorResponse );
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String addInstructorResponse()
	throws Exception
	{
		String response = "";
		int discussionID = -1;
		String instructorResponse = null;
		boolean rc = false;

		if(this.params.size() == 2)
		{
			discussionID = Integer.parseInt(this.getParam("discussionID"));
			instructorResponse = this.getParam("instructorResponse");
		}

		if(discussionID != -1
		&& instructorResponse != null)
		{
			rc = this.discussionDB.addInstructorResponse(discussionID, instructorResponse);
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String addStudentResponse()
	throws Exception
	{
		String response = "";
		int discussionID = -1;
		String studentResponse = null;
		boolean rc = false;

		if(this.params.size() == 2)
		{
			discussionID = Integer.parseInt(this.getParam("discussionID"));
			studentResponse = this.getParam("studentResponse");
		}

		if(discussionID != -1
		&& studentResponse != null)
		{
			rc = this.discussionDB.addStudentResponse(discussionID, studentResponse);
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getDiscussion()
	throws Exception
	{
		String response = "";
		int discussionID = -1;

		if(this.params.size() == 1)
		{
			discussionID = Integer.parseInt(this.getParam("discussionID"));
		}

		if(discussionID != -1)
		{
			String[] columns = this.discussionDB.getDiscussion(discussionID);

			response = String.format
						(	"assignmentID=%s&issueNumber=%s&time=%s&"+
							"summary=%s&details=%s&discussionType=%s&"+
							"studentResponse=%s&instructorResponse=%s",
							columns[0],
							columns[1],
							columns[2],
							columns[3],
							columns[4],
							columns[5],
							columns[6],
							columns[7] );
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String deleteDiscussion()
	throws Exception
	{
		String response = "";
		int discussionID = -1;

		if(this.params.size() == 1)
		{
			discussionID = Integer.parseInt(this.getParam("discussionID"));
		}

		if(discussionID != -1
		&& this.discussionDB.deleteDiscussion(discussionID))
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createLectures()
	throws Exception
	{
		String response = "";
		String email = null;
		int courseID = -1;

		if(this.params.size() == 1)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			courseID = Integer.parseInt(this.getParam("courseID"));
		}

		if(email != null && courseID != -1)
		{
			this.lecturesDB.createLectures(email, courseID);

			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String deleteLectures()
	throws Exception
	{
		String response = "";
		String email = null;
		int courseID = -1;

		if(this.params.size() == 1)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			courseID = Integer.parseInt(this.getParam("courseID"));
		}

		if(email != null
		&& courseID != -1
		&& this.lecturesDB.deleteLectures(email, courseID))
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createTakes()
	throws Exception
	{
		String response = "";
		String email = null;
		int courseID = -1;

		if(this.params.size() == 2)
		{
			email = this.getParam("email");
			courseID = Integer.parseInt(this.getParam("courseID"));
		}

		if(email != null && courseID != -1)
		{
			this.takesDB.createTakes(email, courseID);

			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String deleteTakes()
	throws Exception
	{
		String response = "";
		String email = null;
		int courseID = -1;

		if(this.params.size() == 2)
		{
			email = this.getParam("email");
			courseID = Integer.parseInt(this.getParam("courseID"));
		}

		if(email != null
		&& courseID != -1
		&& this.takesDB.deleteTakes(email, courseID))
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String createWorksOn()
	throws Exception
	{
		String response = "";
		String email = null;
		int assignmentID = -1;
		double grade = -1;

		if(this.params.size() == 3)
		{
			email = this.getParam("email");
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
			grade = Double.parseDouble(this.getParam("grade"));
		}

		if(email != null && assignmentID != -1) // TODO are we allowing negative grades?
		{
			this.worksOnDB.createWorksOn(email, assignmentID, grade);

			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String updateWorksOn()
	throws Exception
	{
		String response = "";
		String email = null;
		int assignmentID = -1;
		double grade = -1;
		boolean rc = false;

		if(this.params.size() == 3)
		{
			email = this.getParam("email");
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
			grade = Double.parseDouble(this.getParam("grade"));
		}

		if(email != null && assignmentID != -1)
		{
			rc = this.worksOnDB.updateWorksOn(email, assignmentID, grade);
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getWorksOn()
	throws Exception
	{
		String response = "";
		String email = null;
		int assignmentID = -1;

		if(this.params.size() == 2)
		{
			email = this.getParam("email");
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
		}

		if(email != null && assignmentID != -1)
		{
			String[] columns = this.worksOnDB.getWorksOn(email, assignmentID);

			response = String.format
						(	"grade=%s",
							columns[0] );
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String deleteWorksOn()
	throws Exception
	{
		String response = "";
		String email = null;
		int assignmentID = -1;

		if(this.params.size() == 2)
		{
			email = this.getParam("email");
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
		}

		if(email != null
		&& assignmentID != -1
		&& this.worksOnDB.deleteWorksOn(email, assignmentID))
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getGrades()
	throws Exception
	{
		String response = "";
		String email = null;

		if(this.params.size() == 0)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());

			System.err.println(email);
		}

		if(email != null)
		{
			ArrayList<String[]> results = this.worksOnDB.getGrades(email);
			String previousCourseName = "";
			for (int i = 0; i < results.size(); ++i) {
				if (!results.get(i)[0].equals(previousCourseName)) {
					previousCourseName = results.get(i)[0];
					if (i != 0) {
						response += "&";
					}
					response += String.format("courseName=%s&averageGrade=%s", results.get(i)[0], results.get(i)[1]);
				}
				response += String.format("&assignmentName=%s&grade=%s", results.get(i)[2], results.get(i)[3]);
			}
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String assignGrade()
	throws Exception
	{
		String response = "";
		String email = null;
		String courseYear = null;
		String courseSemester = null;
		String courseName = null;
		String description = null;
		double grade = -1;
		boolean rc = false;

		if(this.params.size() == 6)
		{
			email = this.getParam("email");
			courseYear = this.getParam("year");
			courseSemester = this.getParam("semester");
			courseName = this.getParam("courseName");
			description = this.getParam("description");
			grade = Double.parseDouble(this.getParam("grade"));
		}

		if(email != null
		&& courseYear != null
		&& courseSemester != null
		&& courseName != null
		&& description != null)
		{
			this.worksOnDB.assignGrade(email, courseYear, courseSemester, courseName, description, grade);
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String addCourse()
	throws Exception
	{
		String response = "";
		String email = null;
		String courseName = null;
		int year = -1;
		String semester = null;

		if(this.params.size() == 3)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			courseName = this.getParam("courseName");
			year = Integer.parseInt(this.getParam("year"));
			semester = this.getParam("semester");
		}

		if(courseName != null && year != -1 && semester != null && this.accountDB.getInstructor(email).length() > 0)
		{
			int rc = this.courseDB.createCourse(courseName, year, semester);
			if(email != null && rc != -1) {
				this.lecturesDB.createLectures(email, rc);
				response = SUCCESS_STR;
			}
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String enrollCourse()
	throws Exception
	{
		String response = "";
		String email = null;
		String courseName = null;
		int year = -1;
		String semester = null;

		if(this.params.size() == 3)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			courseName = this.getParam("courseName");
			year = Integer.parseInt(this.getParam("year"));
			semester = this.getParam("semester");
		}

		if(courseName != null && year != -1 && semester != null && email != null)
		{
			this.takesDB.enrollCourse(email, year, semester, courseName);

			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String addAssignment()
	throws Exception
	{
		String email = accountDB.getEmailFromHash(this.getHashFromCookie());
		if (this.accountDB.getInstructor(email).length() > 0) {
			String response = createAssignment();
			if (Integer.parseInt(response) > 0) {
				return SUCCESS_STR;
			}
		}
		return "";
	}

	private String addNewDiscussion()
	throws Exception
	{

		String response = "";
		int assignmentID = -1;
		String time = null;
		String type = null;
		String summary = null;
		String details = null;

		if(this.params.size() == 5)
		{
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
			time = this.getParam("time");
			type = this.getParam("type");
			summary = this.getParam("summary");
			details = this.getParam("details");
		}

		if(assignmentID != -1
		&& time != null
		&& type != null
		&& summary != null
		&& details != null)
		{
			String rc = this.discussionDB.addNewDiscussion
						(	assignmentID,
							time,
							type,
							summary,
							details );
			response = "issueNumber=" + rc;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String addFollowupDiscussion()
	throws Exception
	{

		String response = "";
		int issueNumber = -1;
		String time = null;
		String type = null;
		String summary = null;
		String details = null;

		if(this.params.size() == 3)
		{
			issueNumber = Integer.parseInt(this.getParam("issueNumber"));
			time = this.getParam("time");
			type = "followup";
			summary = "";
			details = this.getParam("details");
		}

		if(issueNumber != -1
		&& time != null
		&& type != null
		&& summary != null
		&& details != null)
		{
			int rc = this.discussionDB.addFollowupDiscussion
						(	issueNumber,
							time,
							type,
							summary,
							details );
			//response = "" + rc;
			return SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String addResponse()
	throws Exception
	{
		String response = "";
		String email = null;
		int discussionID = -1;
		String text = null;
		boolean rc = false;

		if(this.params.size() == 2)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
			discussionID = Integer.parseInt(this.getParam("discussionID"));
			text = this.getParam("text");
		}

		if(email != null && discussionID != -1 && text != null)
		{
			if (this.accountDB.getInstructor(email).length() > 0) {
				rc = this.discussionDB.addInstructorResponse(discussionID, text);
			} else if (this.accountDB.getStudent(email).length() > 0) {
				rc = this.discussionDB.addStudentResponse(discussionID, text);
			}
		}

		if(rc)
		{
			response = SUCCESS_STR;
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getCourses()
	throws Exception
	{
		String response = "";
		String email = null;

		if(this.params.size() == 0)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
		}

		if(email != null)
		{
			ArrayList<String[]> results = this.takesDB.getCourses(email);
			for (int i = 0; i < results.size(); ++i) {
				if (response.length() != 0) {
					response += "&";
				}
				response += String.format("courseID=%s&courseName=%s", results.get(i)[0], results.get(i)[1]);
			}
			results = this.lecturesDB.getCourses(email);
			for (int i = 0; i < results.size(); ++i) {
				if (response.length() != 0) {
					response += "&";
				}
				response += String.format("courseID=%s&courseName=%s", results.get(i)[0], results.get(i)[1]);
			}
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getAssignments()
	throws Exception
	{
		String response = "";
		int courseID = -1;

		if(this.params.size() == 1)
		{
			courseID = Integer.parseInt(this.getParam("courseID"));
		}

		if(courseID != -1)
		{
			ArrayList<String[]> results = this.assignmentDB.getAssignments(courseID);
			for (int i = 0; i < results.size(); ++i) {
				if (response.length() != 0) {
					response += "&";
				}
				response += String.format("assignmentID=%s&assignmentName=%s", results.get(i)[0], results.get(i)[1]);
			}
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getDiscussions()
	throws Exception
	{
		String response = "";
		int assignmentID = -1;

		if(this.params.size() == 1)
		{
			assignmentID = Integer.parseInt(this.getParam("assignmentID"));
		}

		if(assignmentID != -1)
		{
			ArrayList<String[]> results = this.discussionDB.getDiscussions(assignmentID);
			String previousIssueNumber = "";
			for (int i = 0; i < results.size(); ++i) {
				if (response.length() != 0) {
					response += "&";
				}
				if (!results.get(i)[0].equals(previousIssueNumber)) {
					previousIssueNumber = results.get(i)[0];
					response += String.format("issueNumber=%s&summary=%s", results.get(i)[0], results.get(i)[1]);
				}
			}
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getDiscussionDetails()
	throws Exception
	{
		String response = "";
		int issueNumber = -1;

		if(this.params.size() == 1)
		{
			issueNumber = Integer.parseInt(this.getParam("issueNumber"));
		}

		if(issueNumber != -1)
		{
			ArrayList<String[]> results = this.discussionDB.getDiscussionDetails(issueNumber);
			for (int i = 0; i < results.size(); ++i) {
				if (response.length() != 0) {
					response += "&";
				}
				response += String.format("type=%s&summary=%s&details=%s&instructorResponse=%s&studentResponse=%s", 
					results.get(i)[0], 
					results.get(i)[1],
					results.get(i)[2],
					results.get(i)[3],
					results.get(i)[4]);
			}
		}
		else
		{
			System.err.println("Warning: RPC exec failed: Invalid arguments.");
		}

		return response;
	}

	private String getAccountType()
	throws Exception
	{
		String email = null;

		if(this.params.size() == 0)
		{
			email = accountDB.getEmailFromHash(this.getHashFromCookie());
		}

		if(email != null)
		{
			if (this.accountDB.getInstructor(email).length() > 0) {
				return "instructor";
			} else if (this.accountDB.getStudent(email).length() > 0) {
				return "student";
			}
		}

		return "";
	}
}
