import java.util.Arrays;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Handles parsing of RPC request string
 */
public class RpcParser
{
	final protected String methodNameStr = "method";

	protected String method;
	protected ArrayList<ArrayList<String>> params;

	public RpcParser()
	{
		// Intentionally left blank
	}

	public RpcParser(String str)
	{
		this.method = "";
		this.params = new ArrayList<ArrayList<String>>();

		this.parse(str);
	}

	public String getMethod()
	{
		return this.method;
	}

	public ArrayList<ArrayList<String>> getParamList()
	{
		return this.params;
	}

	public void parse(String str)
	{
		StringTokenizer tokenizer = new StringTokenizer(str, "&");

		while(tokenizer.hasMoreTokens())
		{
			String[] token = parseToken(tokenizer.nextToken());

			if(token[0].equals(methodNameStr))
			{
				this.method = token[1];
			}
			else
			{
				this.params.add(new ArrayList<String>(Arrays.asList(token)));
			}
		}
	}

	public String getParam(String name)
	{
		String result = null;

		for(int i = 0; i < this.params.size(); i++)
		{
			if(this.params.get(i).get(0).equals(name))
			{
				result = this.params.get(i).get(1);
			}
		}

		return result;
	}

	private String[] parseToken(String str)
	{
		StringTokenizer tokenizer = new StringTokenizer(str, "=");
		String[] result = new String[2];

		for(int i = 0; i < 2; i++)
		{
			result[i] = tokenizer.nextToken();
		}

		if(tokenizer.hasMoreTokens())
		{
			System.err.println
				("Warning: Found malformed token in RPC request.");
		}

		return result;
	}
}
