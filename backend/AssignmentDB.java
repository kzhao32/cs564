import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.util.ArrayList;

public class AssignmentDB extends Database
{
	protected Connection conn;

	public AssignmentDB(String uri) throws Exception
	{
		super(uri);

		final String query  =	"CREATE TABLE IF NOT EXISTS assignment"+
								"(assignmentID SERIAL NOT NULL,"+
								"courseID INTEGER,"+
								"type TEXT,"+
								"assignDate TEXT,"+
								"dueDate TEXT,"+
								"description TEXT,"+
								"PRIMARY KEY (assignmentID),"+
								"FOREIGN KEY(courseID) "+
								"REFERENCES course(courseID));";

		Statement stmnt;

		this.conn = Database.getConnection(uri);
		stmnt = this.conn.createStatement();

		stmnt.execute(query);
	}

	public int createAssignment(	int courseID,
									String type,
									String assignDate,
									String dueDate,
									String description )
	throws Exception
	{
		final String query = "INSERT INTO assignment VALUES(DEFAULT,?,?,?,?,?);";
		PreparedStatement stmnt;
		ResultSet rs;

		stmnt = this.conn.prepareStatement(	query,
											Statement.RETURN_GENERATED_KEYS );

		stmnt.setInt(1, courseID);
		stmnt.setString(2, type);
		stmnt.setString(3, assignDate);
		stmnt.setString(4, dueDate);
		stmnt.setString(5, description);

		stmnt.execute();

		rs = stmnt.getGeneratedKeys();

		// There should only be one key generated
		rs.next();

		return rs.getInt(1);
	}

	public boolean updateAssignment(	int assignmentID,
										int courseID,
										String type,
										String assignDate,
										String dueDate,
										String description )
	throws Exception
	{
		final String query =	"UPDATE assignment "+
								"SET courseID=?,type=?,assignDate=?,dueDate=?,"+
								"description=?"+
								"WHERE assignmentID=?;";

		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setInt(1, courseID);
		stmnt.setString(2, type);
		stmnt.setString(3, assignDate);
		stmnt.setString(4, dueDate);
		stmnt.setString(5, description);
		stmnt.setInt(6, assignmentID);

		return (stmnt.executeUpdate() > 0);
	}

	public String[] getAssignment(int assignmentID)
	throws Exception
	{
		final String query =	"SELECT courseID,type,"+
								"assignDate,dueDate,description "+
								"FROM assignment "+
								"WHERE assignmentID=?;";

		PreparedStatement stmnt;
		ResultSet rs;
		String[] result;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );

		stmnt.setInt(1, assignmentID);

		rs = stmnt.executeQuery();

		// There should be exactly one tuple
		rs.next();

		result = new String[5];

		for(int i = 0; i < 5; i++)
		{
			result[i] = rs.getString(i+1);
		}

		return result;
	}

	public ArrayList<String[]> getAssignments(int courseID)
	throws Exception
	{
		final String query =	"SELECT a.assignmentID, a.description "+
								"FROM assignment a "+
								"WHERE a.courseID=?;";

		PreparedStatement stmnt;
		ResultSet rs;
		ArrayList<String[]> results = new ArrayList<String[]>();
		String[] resultToAdd;

		stmnt = this.conn.prepareStatement(	query,
											ResultSet.TYPE_FORWARD_ONLY,
											ResultSet.CONCUR_READ_ONLY );
		stmnt.setInt(1, courseID);

		rs = stmnt.executeQuery();

		while (rs.next()) {
			resultToAdd = new String[2];
			resultToAdd[0] = "" + rs.getString(1);
			resultToAdd[1] = rs.getString(2);
			results.add(resultToAdd);
		}
		return results;
	}

	public boolean deleteAssignment(int assignmentID)
	throws Exception
	{
		final String query = "DELETE FROM assignment WHERE assignmentID=?;";
		boolean result = false;
		PreparedStatement stmnt;

		stmnt = this.conn.prepareStatement(query);

		stmnt.setInt(1, assignmentID);

		result = (stmnt.executeUpdate() > 0);

		return result;
	}
}
